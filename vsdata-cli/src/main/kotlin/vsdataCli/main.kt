// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
package vsdataCli

import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.int
import kotlinx.serialization.json.Json
import kotlinx.serialization.encodeToString
import libvsdata.AssignmentServiceClient
import libvsdata.MoodleClient
import libvsdata.PostalOwlClient
import libvsdata.RecodexClient
import libvsdata.sis.*

val jsonFmt = Json { allowStructuredMapKeys = true }

class Main : CliktCommand(printHelpOnEmptyArgs = true) {
    override fun run() {}
}

class Sis : CliktCommand(help = """
        Connect to UK SIS
        
        Pass credentials in "username:password" form in env variable VSDATA_SIS_CREDS
        """) {
    companion object {
        var sisClient: SisClient? = null
    }

    override fun run() {
        val creds = System.getenv("VSDATA_SIS_CREDS").split(":")
        sisClient = SisClient(SisClient.Authentication.Cas(creds[0], creds[1]))
        sisClient!!.login()
    }


    class SelfUserInfo : CliktCommand() {
        override fun run() {
            println(jsonFmt.encodeToString(getSelfUserInfo(sisClient!!)))
        }
    }

    class SelfStudyEnrollments : CliktCommand() {
        override fun run() {
            println(jsonFmt.encodeToString(getSelfStudyEnrollments(sisClient!!)))
        }
    }

    class LocalFaculties : CliktCommand() {
        override fun run() {
            println(jsonFmt.encodeToString(getLocalFaculties(sisClient!!)))
        }
    }

    class SelfSchedule : CliktCommand() {
        val year by option().int().required()
        val semester by option()
            .choice("winter" to Semester.Winter, "summer" to Semester.Summer)
            .required()
        val kind by option().switch("--daily" to "daily", "--class-types" to "class-types")

        override fun run() {
            println(
                when(kind) {
                    "daily" ->
                        jsonFmt.encodeToString(getSelfDailySchedule(
                            sisClient!!, AcademicYear(year), semester))
                    "class-types" ->
                        jsonFmt.encodeToString(getSelfScheduleClassTypes(
                            sisClient!!, AcademicYear(year), semester))
                    else ->
                        jsonFmt.encodeToString(getSelfSchedule(
                            sisClient!!, AcademicYear(year), semester))
                })
        }
    }

    class SubjectSchedule : CliktCommand() {
        val year by option().int().required()
        val semester by option()
            .choice("winter" to Semester.Winter, "summer" to Semester.Summer)
            .required()
        val faculty by option().required()
        val subject by option().required()
        val kind by option().switch("--daily" to "daily", "--class-types" to "class-types")

        override fun run() {
            println(
                when(kind) {
                    "daily" ->
                        jsonFmt.encodeToString(getSubjectDailySchedule(
                            sisClient!!, AcademicYear(year), semester, faculty, subject))
                    "class-types" ->
                        jsonFmt.encodeToString(getSubjectScheduleClassTypes(
                            sisClient!!, AcademicYear(year), semester, faculty, subject))
                    else ->
                        jsonFmt.encodeToString(getSubjectSchedule(
                            sisClient!!, AcademicYear(year), semester, faculty, subject))
                })
        }
    }

    class SearchSubjects : CliktCommand() {
        val faculty by option().required()
        val name by option()
        val id by option()
        val exactMatch by option().flag()

        override fun run() {
            println(jsonFmt.encodeToString(searchSubjects(
                sisClient!!, faculty, name, id, exactMatch)))
        }
    }

    class SubjectDetail : CliktCommand() {
        val id by option().required()

        override fun run() {
            println(jsonFmt.encodeToString(getSubject(sisClient!!, id)))
        }
    }

    class ScheduledClassDetails : CliktCommand() {
        val year by option().int().required()
        val semester by option()
            .choice("winter" to Semester.Winter, "summer" to Semester.Summer)
            .required()
        val faculty by option().required()
        val id by option().required()

        override fun run() {
            println(jsonFmt.encodeToString(getScheduledClassDetails(
                sisClient!!, AcademicYear(year), semester, faculty, id)))
        }
    }

    class YearSchedule : CliktCommand() {
        val eventType by option().required()

        override fun run() {
            println(jsonFmt.encodeToString(getYearSchedule(sisClient!!, eventType)))
        }
    }

    class SemesterStartDates : CliktCommand() {
        override fun run() {
            println(jsonFmt.encodeToString(getSemesterStartDates(sisClient!!)))
        }
    }
}

class Assignments : CliktCommand(help = """
        Read assignments from various sources
    
        For Postal Owl, pass credentials in "username:password" form in env variable VSDATA_OWL_CREDS
            
        For Recodex, pass credentials in "username:password" form in env variable VSDATA_RECODEX_CREDS

        For Moodle, pass the base URL in env variable VSDATA_MOODLE_URL and the credentials
        in "username:password" form in env variable VSDATA_MOODLE_CREDS
        """) {
    companion object {
        var client: AssignmentServiceClient? = null
    }

    val service by option().choice("postal-owl", "recodex", "moodle")

    override fun run() {
        client = when(service) {
            "postal-owl" -> {
                val creds = System.getenv("VSDATA_OWL_CREDS").split(":")
                val c = PostalOwlClient(creds[0], creds[1])
                c.login()
                c
            }
            "recodex" -> {
                val creds = System.getenv("VSDATA_RECODEX_CREDS").split(":")
                val c = RecodexClient(RecodexClient.Authentication.Cas(creds[0], creds[1]))
                c.login()
                c
            }
            "moodle" -> {
                val url = System.getenv("VSDATA_MOODLE_URL")
                val creds = System.getenv("VSDATA_MOODLE_CREDS").split(":")
                val c = MoodleClient(url, creds[0], creds[1])
                c.login()
                c
            }
            else -> throw Exception()
        }
    }

    class Courses : CliktCommand() {
        override fun run() {
            println(jsonFmt.encodeToString(client!!.getCourses()))
        }
    }

    class AssignmentList : CliktCommand() {
        val course by option().required()

        override fun run() {
            println(jsonFmt.encodeToString(client!!.getAssignments(course)))
        }
    }

    class AssignmentPosts : CliktCommand() {
        val assignment by option().required()

        override fun run() {
            println(jsonFmt.encodeToString(client!!.getAssignmentPosts(assignment)))
        }
    }
}

fun main(args: Array<String>) =
    Main()
        .subcommands(
            Sis()
                .subcommands(
                    Sis.SelfUserInfo(),
                    Sis.SelfStudyEnrollments(),
                    Sis.LocalFaculties(),
                    Sis.SelfSchedule(),
                    Sis.SubjectSchedule(),
                    Sis.SearchSubjects(),
                    Sis.SubjectDetail(),
                    Sis.ScheduledClassDetails(),
                    Sis.YearSchedule(),
                    Sis.SemesterStartDates(),
                ),
            Assignments()
                .subcommands(
                    Assignments.Courses(),
                    Assignments.AssignmentList(),
                    Assignments.AssignmentPosts(),
                ),
        )
        .main(args)
