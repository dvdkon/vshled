// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.outlined.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.google.android.material.datepicker.MaterialDatePicker
import libvsdata.sis.*
import vshled.android.*
import vshled.android.ui.*
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@Composable
fun DailyScheduleHeader(dayInWeek: LocalDate) {
    var weekStart = dayInWeek
    while(weekStart.dayOfWeek != DayOfWeek.MONDAY) {
        weekStart = weekStart.minusDays(1)
    }

    val picker = remember {
        val picker = MaterialDatePicker.Builder
            .datePicker()
            .setSelection(weekStart.atStartOfDay(ZoneId.of("UTC")).toInstant().toEpochMilli())
            .build()
        picker.addOnPositiveButtonClickListener {
            val day = Instant.ofEpochMilli(it).atZone(ZoneId.of("UTC")).toLocalDate()
            GlobalState.screenStack.swapTop(Screen.DailySchedule(day))
        }
        picker
    }

    Row(verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.clickable {
            picker.show(GlobalState.activity!!.supportFragmentManager, "Pick a day")
        }) {
        Text("Daily Schedule")
        Icon(VshledIcons.ArrowDropDown, "Choose date")
    }
}

@Composable
fun DailyScheduleClassCard(
        year: AcademicYear,
        semester: Semester,
        classTypes: Map<String, ClassType>, cls: ScheduledClassDay) {
    val type = classTypes[cls.id]
    Card(elevation = 4.dp,
         backgroundColor = if(type == ClassType.Lecture) primaryRedLight else secondaryAmberLight,
         modifier = Modifier
             .clickable {
                 GlobalState.screenStack.push(
                     Screen.ScheduledClass(year, semester, cls.subjectId, cls.id))
             }
             .fillMaxWidth()
             .padding(smallPadding)
    ) {
        Column(Modifier.padding(smallPadding)) {
            Row(horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()) {
                Text(cls.subjectName,
                     maxLines = 1,
                     overflow = TextOverflow.Ellipsis,
                     modifier = Modifier.weight(5f))
                Text(cls.startTime.toString(),
                     color = greyText,
                     textAlign = TextAlign.End,
                     modifier = Modifier.weight(1f))
            }
            Row(horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()) {
                Text(cls.room ?: "",
                     color = greyText,
                     modifier = Modifier.weight(1f))
                Text((cls.startTime + cls.duration).toString(),
                     color = greyText,
                     textAlign = TextAlign.End,
                     modifier = Modifier.weight(1f))
            }
        }
    }
}

fun dailyScheduleForDay(day: LocalDate) =
    SisStore.getSemesterStartDatesCached().bind { semesters ->
        val semStart = semesters
            .sortedBy { it.date }
            .findLast { it.date.toLocalDate() <= day }!!
        val yr = semStart.academicYear
        val sem = semStart.semester
        SisStore.getSelfDailyScheduleCached(yr, sem)
            .combine(SisStore.getSelfScheduleClassTypesCached(yr, sem))
            .map { Triple(yr, sem, it) } // No Quadruple :(
    }

@Composable
fun DailyScheduleContent(dayInWeek: LocalDate) {
    // TODO: Don't repeat this?
    var weekStart = dayInWeek
    while(weekStart.dayOfWeek != DayOfWeek.MONDAY) {
        weekStart = weekStart.minusDays(1)
    }
    val weekStartDt = LocalDateTime.of(weekStart, LocalTime.of(0, 0))
    val weekEndDt = weekStartDt + Duration.ofDays(7)
    WithCachedResource(dailyScheduleForDay(weekEndDt.toLocalDate())) { (year, semester, data) ->
        Column(Modifier.verticalScroll(rememberScrollState())) {
            val (allClasses, classTypes) = data
            val classes =
                allClasses.filter { it.day >= weekStart && it.day < weekEndDt.toLocalDate() }
            classes.groupBy { it.day }.toList().sortedBy { it.first }.forEach { (day, classes) ->
                Text(day.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)),
                     textAlign = TextAlign.Center,
                     fontWeight = FontWeight.Bold,
                     modifier = Modifier
                         .padding(smallPadding)
                         .fillMaxWidth())
                for(cls in classes) {
                    DailyScheduleClassCard(year, semester, classTypes, cls)
                }
            }
        }
    }
}