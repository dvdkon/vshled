// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import libvsdata.sis.SisClient
import vshled.android.AccountService

@Composable
fun SettingsUkCasAccountHeader() {
    Text("UK CAS Account")
}

@Composable
fun SettingsUkCasAccountContent() {
    AccountLogin(
        AccountService.UkCas,
        AccountLocator.Singular,
        canName = false, canUseCasCreds = false, loginProc = { username, password, _ ->
            val client = SisClient(SisClient.Authentication.Cas(username, password))
            client.login()
        }, onLogin = { id, username, password, _ -> })
}
