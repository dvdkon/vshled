// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.outlined.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import vshled.android.AccountService
import vshled.android.AssignmentStore
import vshled.android.ui.*

@Composable
fun SettingsAssignmentAccountsHeader() {
    var menuShown by remember { mutableStateOf(false) }

    Row(horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.fillMaxWidth()) {
        Text("Assignment Services")
        Box {
            Icon(VshledIcons.Add, "Add service",
                 Modifier
                     .clickable {
                         menuShown = !menuShown
                     }
                     .padding(14.dp))
            DropdownMenu(expanded = menuShown, onDismissRequest = { menuShown = false }) {
                DropdownMenuItem(onClick = {
                    GlobalState.screenStack.push(Screen.SettingsPostalOwlAccount(null))
                }) {
                    Text("Postal Owl")
                }
                DropdownMenuItem(onClick = {
                    GlobalState.screenStack.push(Screen.SettingsRecodexAccount(null))
                }) {
                    Text("Recodex")
                }
                DropdownMenuItem(onClick = {
                    GlobalState.screenStack.push(Screen.SettingsMoodleAccount(null))
                }) {
                    Text("Moodle")
                }
            }
        }
    }
}

@Composable
fun SettingsAssignmentAccountsContent() {
    Column {
        val accounts = rememberRecomputable {
            GlobalState.database!!.accountQueries.selectByServices(listOf(
                AccountService.PostalOwl.toString(),
                AccountService.Recodex.toString(),
                AccountService.Moodle.toString(),
            )).executeAsList()
        }
        for(account in accounts.value) {
            Row(verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .clickable {
                        GlobalState.screenStack.push(
                            when(account.service) {
                                AccountService.PostalOwl.toString() ->
                                    Screen.SettingsPostalOwlAccount(account.id)
                                AccountService.Recodex.toString() ->
                                    Screen.SettingsRecodexAccount(account.id)
                                AccountService.Moodle.toString() ->
                                    Screen.SettingsMoodleAccount(account.id)
                                else -> throw Exception("Unreachable")
                            })
                    }
                    .fillMaxWidth()
                    .padding(start = largePadding, top = tinyPadding, bottom = tinyPadding)
            ) {
                Text("ID ${account.id}", modifier = Modifier.weight(4f))
                Text(AccountService.valueOf(account.service).displayName(),
                     style = typography.subtitle1,
                     modifier = Modifier.weight(5f))
                Icon(VshledIcons.Delete, "Delete account",
                     Modifier
                         .clickable {
                             GlobalScope.launch {
                                 GlobalState.database!!.accountQueries.deleteById(account.id)
                                 GlobalState.assignmentServices.remove(account.id)
                                 accounts.recompute()
                             }
                         }
                         .padding(14.dp))
            }
        }
    }
}

@Composable
fun GroupChooserDialog(accountId: Long, courseId: String, onClose: () -> Unit) {
    val hiddenGroups = rememberRecomputable {
        GlobalState.database!!.assignmentGroupVisibilityQueries
            .hiddenGroups(accountId, courseId).executeAsList()
    }

    AlertDialog(
        onDismissRequest = onClose,
        title = { Text("Choose visible assignment groups") },
        confirmButton = {
            Button(onClick = onClose) {
                Text("Ok")
            }
        },
        text = {
            WithCachedResource(
                AssignmentStore.getAssignmentsCached(accountId, courseId)
            ) { groups ->
                // What we _actually_ want is to say that the dialog
                // shouldn't overflow the screen
                Box(modifier = Modifier.heightIn(max = 300.dp)) {
                    Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
                        for(group in groups) {
                            SpacerSmall()
                            LabeledCheckbox(
                                label = group.name ?: "Unnamed group",
                                checked = !hiddenGroups.value.contains(group.name ?: ""),
                                onCheckedChange = { checked ->
                                    GlobalState.database!!.assignmentGroupVisibilityQueries
                                        .upsertVisibility(
                                            accountId,
                                            courseId,
                                            group.name ?: "",
                                            if(checked) 1 else 0)
                                    hiddenGroups.recompute()
                                })
                        }
                    }
                }
            }
        })
}

@Composable
fun CourseMapping(accountId: Long) {
    WithCachedResource(AssignmentStore.getCoursesCached(accountId)) { courses ->
        LaunchedEffect(accountId) {
            for(course in courses) {
                GlobalState.database!!.assignmentCourseMappingsQueries
                    .insertDefaultBlank(accountId, course.id)
            }
        }

        val coursesWithBindings = rememberRecomputable {
            val bindings =
                GlobalState.database!!.assignmentCourseMappingsQueries
                    .selectByAccount(accountId)
                    .executeAsList()
                    .map { Pair(it.courseId, it) }
                    .toMap()
            courses.map { Pair(it, bindings[it.id]) }
        }

        SpacerMedium()
        Column(Modifier.padding(screenPadding)) {
            Text("Courses", style = typography.h3)
            for((course, binding) in coursesWithBindings.value) {
                var expanded by rememberSaveable { mutableStateOf(false) }
                var subjectChooserShown by rememberSaveable { mutableStateOf(false) }
                var subjectId by rememberSaveable { mutableStateOf(binding?.sisSubjectId ?: "") }
                var groupChooserShown by rememberSaveable { mutableStateOf(false) }

                Row(verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .clickable { expanded = !expanded }
                        .fillMaxWidth()
                        .padding(top = mediumPadding)) {
                    Text(course.name,
                         modifier = Modifier.weight(5f),
                         color = if(binding?.visible == 1L) black else greyText)
                    Icon(if(expanded) VshledIcons.KeyboardArrowUp else VshledIcons.KeyboardArrowDown,
                         if(expanded) "Collapse" else "Expand")
                }

                if(subjectChooserShown) {
                    // TODO: Exception when clicking on the text field then using the back
                    //  button twice (should close the dialog, instead crashes the app)
                    AlertDialog(
                        onDismissRequest = { subjectChooserShown = false },
                        title = { Text("Choose a subject") },
                        confirmButton = {
                            Button({
                                       GlobalState.database!!.assignmentCourseMappingsQueries
                                           .updateSisSubjectId(
                                               accountId = accountId,
                                               courseId = course.id,
                                               sisSubjectId = subjectId)
                                       coursesWithBindings.recompute()
                                   }) { Text("Set") }
                        },
                        text = {
                            TextField(subjectId, { subjectId = it })
                        })
                }

                if(groupChooserShown) {
                    GroupChooserDialog(accountId, course.id) { groupChooserShown = false }
                }

                if(expanded) {
                    Column(Modifier.padding(start = mediumPadding, end = mediumPadding)) {
                        fun onCheckboxClick(checked: Boolean) {
                            GlobalState.database!!.assignmentCourseMappingsQueries
                                .updateVisibility(accountId = accountId, courseId = course.id,
                                                  visible = if(checked) 1L else 0)
                            coursesWithBindings.recompute()
                        }
                        Row(verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier
                                .clickable {
                                    onCheckboxClick(binding?.visible != 1L)
                                }
                                .fillMaxWidth()
                                .height(48.dp)) {
                            Text("Visible: ",
                                 style = typography.subtitle1,
                                 modifier = Modifier.width(100.dp))
                            Checkbox(binding?.visible == 1L, ::onCheckboxClick)
                        }
                        Row(horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(48.dp)) {
                            Row {
                                Text("Bound subject: ", style = typography.subtitle1,
                                     modifier = Modifier.width(100.dp))
                                Text(binding?.sisSubjectId ?: "Not set")
                            }
                            Button({ subjectChooserShown = true }) {
                                //Text("Change")
                                Icon(VshledIcons.Edit, "Change Bound Subject")
                            }
                        }
                        Row(horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(48.dp)) {
                            Text("Group visibility: ", style = typography.subtitle1,
                                 modifier = Modifier.width(100.dp))
                            Button({ groupChooserShown = true }) {
                                //Text("Change")
                                Icon(VshledIcons.Edit, "Change Group Visibility")
                            }
                        }
                    }
                }
            }
        }
    }
}
