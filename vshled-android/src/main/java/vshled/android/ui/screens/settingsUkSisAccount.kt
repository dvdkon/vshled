// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import libvsdata.sis.SisClient
import vshled.android.AccountService
import vshled.android.ui.GlobalState

@Composable
fun SettingsUkSisAccountHeader() {
    Text("UK SIS Account")
}

@Composable
fun SettingsUkSisAccountContent() {
    AccountLogin(
        AccountService.UkSis,
        AccountLocator.Singular,
        canName = false, canUseCasCreds = true, loginProc = { username, password, _ ->
            val client = SisClient(SisClient.Authentication.Cas(username, password))
            client.login()
        }, onLogin = { id, username, password, _ -> GlobalState.loginSis(username, password) })
}
