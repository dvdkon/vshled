// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import libvsdata.AuthenticationFailureException
import vshled.android.*
import vshled.android.ui.*
import kotlin.Exception

@Composable
fun SettingsHeader() {
    Text("Settings")
}

@Composable
fun SettingsContent() {
    Column(Modifier.verticalScroll(rememberScrollState())) {
        SpacerSmall()
        Text("Accounts", style = typography.h3, modifier = Modifier.padding(screenPadding))
        SpacerSmall()
        SettingsLink("UK CAS", Screen.SettingsUkCasAccount)
        SettingsLink("UK SIS", Screen.SettingsUkSisAccount)
        SettingsLink("Assignment Services", Screen.SettingsAssignmentAccounts)
    }
}

@Composable
fun SettingsLink(label: String, screen: Screen) {
    Box(modifier = Modifier
        .clickable { GlobalState.screenStack.push(screen) }
        .fillMaxWidth()
        .padding(start = largePadding, top = mediumPadding, bottom = mediumPadding)
    ) {
        Text(label)
    }
}


enum class LoginState {
    NotSetUp,
    Checking,
    InvalidCreds,
    LoggedIn,
}

sealed class AccountLocator {
    // Don't try to look it up in the DB
    object NewAccount : AccountLocator()

    // There's only ever one account for this service in the DB
    object Singular : AccountLocator()
    class Id(val id: Long) : AccountLocator()
}

fun getAccountByLocator(service: AccountService, locator: AccountLocator) =
    when(locator) {
        is AccountLocator.NewAccount -> null
        is AccountLocator.Singular ->
            GlobalState.database!!.accountQueries
                .selectByService(service.toString())
                .executeAsList()
                .singleOrNull()
        is AccountLocator.Id -> {
            val account = GlobalState.database!!.accountQueries
                .selectById(locator.id)
                .executeAsOne()
            if(account.service != service.toString()) {
                throw Exception("""
                        Account login view called for ID ${locator.id} and
                        service $service, but found service ${account.service}""".trimIndent())
            }
            account
        }
    }

@Composable
fun AccountLogin(
    service: AccountService,
    locator: AccountLocator,
    canName: Boolean,
    canUseCasCreds: Boolean,
    loginProc: (String, String, String?) -> Unit,
    onLogin: (Long, String, String, String?) -> Unit,
    getExtraData: () -> String? = { null },
    extraData: (@Composable () -> Unit)? = {},
) {
    val casAccountFromDb =
        if(!canUseCasCreds) null
        else rememberRecomputable { getCasAccount() }
    val accountFromDb =
        rememberRecomputable(service, locator) { getAccountByLocator(service, locator) }

    var name by rememberSaveable {
        mutableStateOf(accountFromDb.value?.name ?: "")
    }
    var useCasCreds by rememberSaveable {
        mutableStateOf(
            if(!canUseCasCreds) false
            else if(casAccountFromDb!!.value == null) false
            else accountFromDb.value?.useCredsFrom == casAccountFromDb.value!!.id)
    }
    var username by rememberSaveable {
        mutableStateOf(accountFromDb.value?.username ?: "")
    }
    var password by rememberSaveable {
        mutableStateOf(accountFromDb.value?.password ?: "")
    }

    fun tryLogin(username: String, password: String, setter: (LoginState) -> Unit): Job {
        setter(LoginState.Checking)
        return GlobalScope.launch {
            try {
                loginProc(username, password, getExtraData())
                setter(LoginState.LoggedIn)
            } catch(e: AuthenticationFailureException) {
                setter(LoginState.InvalidCreds)
            }
        }
    }

    var loginState by rememberSaveable {
        val ls = mutableStateOf(LoginState.Checking)
        if(locator is AccountLocator.Id && GlobalState.assignmentServices.containsKey(locator.id)) {
            ls.value = LoginState.LoggedIn
        } else if(accountFromDb.value == null) {
            ls.value = LoginState.NotSetUp
        } else {
            tryLogin(accountFromDb.value!!.username!!, accountFromDb.value!!.password!!) {
                ls.value = it
            }
        }
        ls
    }

    fun loginOnClick() {
        GlobalScope.launch {
            val db = GlobalState.database!!
            val nameOrNull = if(canName) name.ifBlank { null } else null
            val extra = getExtraData()
            if(useCasCreds) {
                val uname = casAccountFromDb!!.value!!.username!!
                val pw = casAccountFromDb.value!!.password!!
                tryLogin(uname, pw) { loginState = it }.join()

                if(loginState == LoginState.LoggedIn) {
                    if(accountFromDb.value != null) {
                        val id = accountFromDb.value!!.id
                        db.accountQueries.updateCreds(
                            id = id, name = nameOrNull,
                            useCredsFrom = casAccountFromDb.value!!.id,
                            username = null, password = null, extra = extra)
                        onLogin(id, uname, pw, extra)
                    } else {
                        db.accountQueries.transaction {
                            db.accountQueries.insert(
                                service.toString(),
                                nameOrNull,
                                casAccountFromDb.value!!.id,
                                null,
                                null,
                                extra)
                            onLogin(db.accountQueries.lastInsertedId().executeAsOne(), uname, pw,
                                    extra)
                        }
                        accountFromDb.recompute()
                    }
                }
            } else {
                tryLogin(username, password) { loginState = it }.join()

                if(loginState == LoginState.LoggedIn) {
                    if(accountFromDb.value != null) {
                        val id = accountFromDb.value!!.id
                        db.accountQueries.updateCreds(
                            id = id, name = nameOrNull, useCredsFrom = null,
                            username = username, password = password, extra = extra)
                        onLogin(id, username, password, extra)
                    } else {
                        db.accountQueries.transaction {
                            db.accountQueries.insert(
                                service.toString(), nameOrNull, null, username,
                                password, extra)
                            val id = db.accountQueries.lastInsertedId().executeAsOne()
                            onLogin(id, username, password, extra)
                        }
                        accountFromDb.recompute()
                    }
                }
            }
        }
    }

    Column(Modifier.padding(screenPadding)) {
        if(canName) {
            SpacerMedium()
            TextField(name, { name = it },
                      label = { Text("Account name (optional)") },
                      modifier = Modifier.fillMaxWidth())
        }
        SpacerMedium()
        Row {
            Text("State: ", color = greyText)
            Text(when(loginState) {
                     LoginState.NotSetUp -> "Not set up"
                     LoginState.Checking -> "Checking..."
                     LoginState.InvalidCreds -> "Invalid credentials!"
                     LoginState.LoggedIn -> "Logged in!"
                 })
        }
        if(extraData != null) {
            SpacerMedium()
            extraData()
        }
        SpacerMedium()
        if(canUseCasCreds && casAccountFromDb!!.value != null) {
            LabeledCheckbox("Use UK CAS credentials", useCasCreds, { useCasCreds = it })
            SpacerSmall()
        }
        if(!useCasCreds) {
            TextField(value = username,
                      onValueChange = { username = it },
                      label = { Text("Username") },
                      modifier = Modifier.fillMaxWidth())
            SpacerSmall()
            TextField(value = password,
                      onValueChange = { password = it },
                      label = { Text("Password") },
                      visualTransformation = PasswordVisualTransformation(),
                      keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                      modifier = Modifier.fillMaxWidth())
            SpacerSmall()
        }
        Button(onClick = { loginOnClick() }) {
            Text("Login and save")
        }
    }
}
