// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import libvsdata.Assignment
import vshled.android.*
import vshled.android.ui.*
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@Composable
fun AssignmentDetailHeader() {
    Text("Assignment")
}

@Composable
fun AssignmentDetailContent(serviceId: Long, assignment: Assignment) {
    val dateFmt = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
    val isHidden = rememberRecomputable {
        GlobalState.database!!.assignmentVisibilityQueries
            .isHidden(serviceId, assignment.id).executeAsOne()
    }

    WithCachedResource(AssignmentStore.getAssignmentPostsCached(serviceId, assignment.id)) {
        Column(Modifier.verticalScroll(rememberScrollState())) {
            Column(Modifier.padding(screenPadding)) {
                SpacerSmall()
                Text(assignment.name, style = typography.h1)
                Text("$serviceId / ${assignment.id}", style = typography.subtitle1)

                if(assignment.pointsGained != null) {
                    LabeledValue("Points gained", prettyDouble(assignment.pointsGained!!))
                }
                if(assignment.deadlines.isNotEmpty()) {
                    SpacerMedium()
                    FieldLabel("Deadlines")
                    for(deadline in assignment.deadlines) {
                        val dateStr = deadline.dateTime.format(dateFmt)
                        Text("${prettyDouble(deadline.pointsMax)} points, $dateStr")
                    }
                }
                SpacerMedium()
                LabeledCheckbox("Hidden", isHidden.value, { checked ->
                    GlobalState.database!!.assignmentVisibilityQueries
                        .upsertVisibility(serviceId, assignment.id, if(checked) 0 else 1)
                    isHidden.recompute()
                })
            }

            for(post in it) {
                Card(elevation = 4.dp, modifier = Modifier
                    .fillMaxWidth()
                    .padding(mediumPadding)) {
                    Column(Modifier.padding(mediumPadding)) {
                        if(post.author != null || post.postedOn != null) {
                            Row(horizontalArrangement = Arrangement.SpaceBetween,
                                modifier = Modifier.fillMaxWidth()) {
                                Text(post.author ?: "", style = typography.subtitle1)
                                Text(post.postedOn?.format(dateFmt) ?: "",
                                     style = typography.subtitle1,
                                     textAlign = TextAlign.End)
                            }
                            SpacerMedium()
                        }
                        Text(post.body)
                    }
                }
            }
        }
    }
}
