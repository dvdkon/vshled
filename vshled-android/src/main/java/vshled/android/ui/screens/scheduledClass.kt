// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import libvsdata.sis.AcademicYear
import libvsdata.sis.ScheduledClassDetail
import libvsdata.sis.ScheduledWeeks
import libvsdata.sis.Semester
import vshled.android.*
import vshled.android.ui.*
import java.time.format.TextStyle
import java.util.*

@Composable
fun ScheduledClassHeader(scheduledClassId: String) {
    Text("Scheduled Class $scheduledClassId")
}

@Composable
fun ScheduledClassContent(
    year: AcademicYear, semester: Semester, subjectId: String, scheduledClassId: String,
) {
    WithCachedResource(SisStore.getSubjectCached(subjectId).bind { subject ->
        SisStore.getScheduledClassDetailsCached(
            year, semester, subject.faculty.id, scheduledClassId)
    }) { schClasses ->
        Column(Modifier.verticalScroll(rememberScrollState())) {
            // The subject id and name are common between all of them
            // Maybe I should change the libvsdata API?
            Column(Modifier.padding(screenPadding)) {
                val first = schClasses.first()
                Text(first.subject.name, style = typography.h1)
                Text(first.subject.id + " / " + first.id, style = typography.subtitle1)
            }

            for(schClass in schClasses) {
                ScheduledClass(schClass)
            }
        }
    }
}

@Composable
fun ScheduledClass(schClass: ScheduledClassDetail) {
    Card(elevation = 4.dp, modifier = Modifier
        .fillMaxWidth()
        .padding(mediumPadding)) {
        Column(modifier = Modifier.padding(mediumPadding)) {
            Text(schClass.subId, style = typography.h2)
            LabeledValue("Type", schClass.type.toString())
            LabeledValue("Scheduled",
                         when(schClass.scheduledWeeks) {
                             ScheduledWeeks.AllWeeks -> "weekly"
                             ScheduledWeeks.EvenWeeks -> "all even weeks"
                             ScheduledWeeks.OddWeeks -> "all odd weeks"
                             ScheduledWeeks.Irregular -> "irregularly"
                         })
            LabeledValue("Weekday",
                         schClass.weekday.getDisplayName(
                             TextStyle.FULL, Locale.getDefault()))
            LabeledValue("Starting at", schClass.startTime.toString())
            LabeledValue("Room", schClass.room)
            SpacerMedium()
            FieldLabel("Teachers")
            for(teacher in schClass.teachers) {
                Text(teacher.name)
                SpacerSmall()
            }
        }
    }
}