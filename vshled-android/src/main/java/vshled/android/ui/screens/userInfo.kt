// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import vshled.android.*
import vshled.android.ui.LabeledValue
import vshled.android.ui.WithCachedResource
import vshled.android.ui.screenPadding

@Composable
fun SisUserInfoHeader() {
    Text("User Info")
}

@Composable
fun SisUserInfoContent() {
    WithCachedResource(SisStore.getSelfUserInfoCached()) { userInfo ->
        Column(Modifier.padding(screenPadding)) {
            LabeledValue("Name",
                         listOfNotNull(userInfo.degreePrefix, userInfo.firstName,
                                       userInfo.middleName, userInfo.surname,
                                       userInfo.degreeSuffix)
                             .joinToString(" "))
            LabeledValue("UKČO", userInfo.personId?.toString())
            LabeledValue("Primary address", userInfo.addresses[0])
            LabeledValue("Contact address", userInfo.contactAddress)
            LabeledValue("Mobile phone number", userInfo.mobilePhoneNum)
            LabeledValue("Personal email", userInfo.personalEmail)
        }
    }
}
