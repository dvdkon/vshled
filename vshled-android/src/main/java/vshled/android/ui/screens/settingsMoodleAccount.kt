// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import libvsdata.AuthenticationFailureException
import libvsdata.MoodleClient
import vshled.android.AccountService
import vshled.android.ui.GlobalState
import vshled.android.ui.Screen
import vshled.android.ui.SpacerMedium
import vshled.android.ui.mediumPadding

@Composable
fun SettingsMoodleAccountHeader() {
    Text("Moodle Account")
}

@Composable
fun SettingsMoodleAccountContent(accountId: Long?) {
    var serverUrl by rememberSaveable {
        mutableStateOf(
            if(accountId != null)
                GlobalState.database!!.accountQueries.selectById(accountId).executeAsOne().extra!!
            else ""
        )
    }

    Column(Modifier.verticalScroll(rememberScrollState())) {
        // TODO: This abstraction seems to be getting a bit unwieldy
        // But what to replace it with?
        AccountLogin(
            service = AccountService.Moodle,
            locator = if(accountId == null) AccountLocator.NewAccount
            else AccountLocator.Id(accountId),
            canName = true, canUseCasCreds = true,
            extraData = {
                TextField(value = serverUrl,
                          onValueChange = { serverUrl = it },
                          label = { Text("Server URL") },
                          modifier = Modifier.fillMaxWidth())
            },
            getExtraData = { serverUrl },
            loginProc = { username, password, serverUrl ->
                try {
                    val client = MoodleClient(serverUrl!!, username, password)
                    client.login()
                } catch(e: Exception) {
                    // TODO: More granular errors
                    // Might require rewrite of login screens
                    throw AuthenticationFailureException()
                }
            }, onLogin = { id, username, password, serverUrl ->
                val client = MoodleClient(serverUrl!!, username, password)
                client.login()
                GlobalState.assignmentServices[id] = client
                if(accountId == null) {
                    GlobalState.screenStack.swapTop(Screen.SettingsMoodleAccount(id))
                }
            })

        if(accountId != null) {
            Divider(modifier = Modifier.padding(top = mediumPadding))
            CourseMapping(accountId)
        }
        SpacerMedium()
    }
}