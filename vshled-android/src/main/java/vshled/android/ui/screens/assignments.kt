// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.GenericShape
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import libvsdata.Assignment
import libvsdata.AssignmentGroup
import libvsdata.Course
import vshled.android.*
import vshled.android.ui.Screen.Assignments.Grouping
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.outlined.*
import vshled.android.ui.*
import java.time.LocalDateTime

@Composable
fun AssignmentsHeader(grouping: Grouping, showHidden: Boolean) {
    var menuShown by remember { mutableStateOf(false) }

    Row(horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.fillMaxWidth()) {
        Text("Assignments")
        Row {
            if(showHidden)
                Icon(VshledIcons.Visibility, "Showing hidden assignments",
                     Modifier
                         .clickable {
                             GlobalState.screenStack.swapTop(Screen.Assignments(grouping,
                                                                                !showHidden))
                         }
                         .padding(14.dp))
            else
                Icon(VshledIcons.VisibilityOff, "Not showing hidden assignments",
                     Modifier
                         .clickable {
                             GlobalState.screenStack.swapTop(Screen.Assignments(grouping,
                                                                                !showHidden))
                         }
                         .padding(14.dp))
            Box {
                Icon(VshledIcons.Segment, "Change grouping",
                     Modifier
                         .clickable { menuShown = !menuShown }
                         .padding(14.dp))
                DropdownMenu(expanded = menuShown, onDismissRequest = { menuShown = false }) {
                    DropdownMenuItem(onClick = {
                        GlobalState.screenStack.swapTop(
                            Screen.Assignments(Grouping.ByService, showHidden))
                        menuShown = false
                    }) {
                        Text("By Service")
                    }
                    DropdownMenuItem(onClick = {
                        GlobalState.screenStack.swapTop(
                            Screen.Assignments(Grouping.ByCourse, showHidden))
                        menuShown = false
                    }) {
                        Text("By Course")
                    }
                    DropdownMenuItem(onClick = {
                        GlobalState.screenStack.swapTop(
                            Screen.Assignments(Grouping.All, showHidden))
                        menuShown = false
                    }) {
                        Text("All Assignments")
                    }
                }
            }
        }
    }
}

@Composable
fun AssignmentsContent(grouping: Grouping, showHidden: Boolean) {
    val hiddenMap = remember {
        GlobalState.database!!.assignmentVisibilityQueries.hiddenAssignments()
            .executeAsList()
            .groupBy { it.accountId }
            .map { (k, v) -> Pair(k, v.map { it.assignmentId }) }
            .toMap()
    }
    fun hiddenByAccount(accountId: Long) = hiddenMap.getOrDefault(accountId, listOf())

    when(grouping) {
        Grouping.ByService ->
            Column(Modifier.verticalScroll(rememberScrollState())) {
                GlobalState.assignmentServices.keys.forEach { serviceId ->
                    AssignmentService(serviceId, hiddenByAccount(serviceId))
                }
            }
        Grouping.ByCourse -> {
            val visibleCourses = remember {
                GlobalState.database!!.assignmentCourseMappingsQueries
                    .visibleCourses(::Pair)
                    .executeAsList()
            }
            Column(Modifier.verticalScroll(rememberScrollState())) {
                WithCachedResource(
                    combineCachedValuePtrList(
                        GlobalState.assignmentServices.keys.map { serviceId ->
                            AssignmentStore.getCoursesCached(serviceId)
                                .map { Pair(serviceId, it) }
                        })
                ) { xs ->
                    xs.forEach { (serviceId, courses) ->
                        courses
                            .filter { visibleCourses.contains(Pair(serviceId, it.id)) }
                            .forEach { course ->
                                AssignmentCourse(serviceId, course, hiddenByAccount(serviceId))
                            }
                    }
                }
            }
        }
        Grouping.All -> {
            val visibleCourses = remember {
                GlobalState.database!!.assignmentCourseMappingsQueries
                    .visibleCourses(::Pair)
                    .executeAsList()
            }
            WithCachedResource(
                combineCachedValuePtrList(
                    GlobalState.assignmentServices.keys.map { serviceId ->
                        AssignmentStore.getCoursesCached(serviceId)
                            .bind { courses ->
                                combineCachedValuePtrList(courses.map { course ->
                                    AssignmentStore.getAssignmentsCached(serviceId, course.id)
                                        .map { Triple(serviceId, course.id, it) }
                                })
                            }
                    })
            ) { xs ->
                val now = LocalDateTime.now()
                // Unfortunately LazyColumn doesn't deal well with nested data, so we have
                // to do this "id pushdown dance"
                // (that's also why it's only used here)
                val assignments = xs
                    .flatten()
                    .filter { (si, ci, _) -> visibleCourses.contains(Pair(si, ci)) }
                    .flatMap { (si, _, gs) -> gs.map { Pair(si, it) } }
                    .flatMap { (si, ass) -> ass.assignments.map { Pair(si, it) } }
                    .sortedBy { (_, a) ->
                        a.deadlines.map { it.dateTime }.find { it > now } ?: LocalDateTime.MAX
                    }
                LazyColumn(contentPadding = screenPadding) {
                    item { SpacerSmall() }
                    items(assignments) { (serviceId, assignment) ->
                        val hidden = assignment.id !in hiddenByAccount(serviceId)
                        if(hidden && showHidden) {
                            AssignmentCard(serviceId, assignment, true)
                        } else if(!hidden) {
                            AssignmentCard(serviceId, assignment, false)
                        }
                    }
                    item { SpacerSmall() }
                }
            }
        }
    }
}

@Composable
fun AssignmentService(serviceId: Long, hiddenIds: List<String>) {
    val account = remember {
        GlobalState.database!!.accountQueries.selectById(serviceId).executeAsOne()
    }

    var expanded by rememberSaveable { mutableStateOf(false) }
    var invalidate by remember { mutableStateOf(false) }

    Row(horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .clickable { expanded = !expanded }
            .fillMaxWidth()
            .border(1.dp, greyText, shape = GenericShape { size, _ ->
                moveTo(0f, size.height - 4f)
                lineTo(size.width, size.height - 4f)
                lineTo(size.width, size.height)
                lineTo(0f, size.height)
            })
            .background(greyBg)) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            val serviceName = AccountService.valueOf(account.service).displayName()
            Text(account.name ?: ("$serviceName (ID $serviceId)"),
                 style = typography.h2,
                 modifier = Modifier.padding(start = 14.dp))
            Icon(
                if(expanded) VshledIcons.KeyboardArrowUp else VshledIcons.KeyboardArrowDown,
                if(expanded) "Collapse" else "Expand",
                modifier = Modifier.padding(14.dp))
        }
        Icon(VshledIcons.Refresh, "Reload",
             modifier = Modifier
                 .clickable { invalidate = true }
                 .padding(14.dp))
    }

    if(expanded) {
        val visibleCourses = remember {
            GlobalState.database!!.assignmentCourseMappingsQueries
                .visibleCourses(::Pair)
                .executeAsList()
        }
        WithResource(
            {
                val v = AssignmentStore.getCoursesCached(serviceId)(invalidate).value
                if(invalidate) invalidate = false
                v
            }) { courses ->
            courses
                .filter { visibleCourses.contains(Pair(serviceId, it.id)) }
                .forEach { course ->
                    AssignmentCourse(serviceId, course, hiddenIds)
                }
        }
    }
}

@Composable
fun AssignmentCourse(serviceId: Long, course: Course, hiddenIds: List<String>) {
    var expanded by rememberSaveable { mutableStateOf(false) }
    var invalidate by rememberSaveable { mutableStateOf(false) }

    Row(horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .clickable { expanded = !expanded }
            .fillMaxWidth()
            .border(1.dp, secondaryVariantAmber, shape = GenericShape { size, _ ->
                moveTo(0f, size.height - 4f)
                lineTo(size.width, size.height - 4f)
                lineTo(size.width, size.height)
                lineTo(0f, size.height)
            })
            .background(secondaryAmber)) {
        Text(course.name,
             style = typography.h3, color = black,
             modifier = Modifier
                 .padding(start = 24.dp)
                 .weight(4f))
        Row(verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.End,
            modifier = Modifier.weight(2f)) {
            Icon(if(expanded) VshledIcons.KeyboardArrowUp else VshledIcons.KeyboardArrowDown,
                 if(expanded) "Collapse" else "Expand",
                 modifier = Modifier.padding(start = 14.dp))
            Icon(VshledIcons.Refresh, "Reload",
                 modifier = Modifier
                     .clickable { invalidate = true }
                     .padding(14.dp))
        }
    }

    if(expanded) {
        WithResourceKeyed(
            {
                val v = AssignmentStore.getAssignmentsCached(serviceId, course.id)(invalidate).value
                if(invalidate) invalidate = false
                v

            }, arrayOf(invalidate)) { assignmentGroups ->
            Column(Modifier
                       .padding(screenPadding)
                       .padding(bottom = mediumPadding)) {
                assignmentGroups.forEach {
                    AssignmentGroup(serviceId, course.id, it, hiddenIds)
                }
            }
        }
    }
}

@Composable
fun AssignmentGroup(
    serviceId: Long, courseId: String, assignmentGroup: AssignmentGroup,
    hiddenIds: List<String>,
) {
    val isHidden = remember {
        GlobalState.database!!.assignmentGroupVisibilityQueries
            .visibilityForGroup(serviceId, courseId, assignmentGroup.name ?: "")
            .executeAsOneOrNull() == 0L
    }
    if(isHidden) return

    SpacerSmall()

    if(assignmentGroup.name != null)
        Text(assignmentGroup.name!!,
             style = typography.h3,
             modifier = Modifier
                 .padding(smallPadding)
                 .fillMaxWidth())

    Column {
        assignmentGroup.assignments
            .filter { it.id !in hiddenIds }
            .forEach { AssignmentCard(serviceId, it, false) }
    }
}

@Composable
fun AssignmentCard(serviceId: Long, assignment: Assignment, faded: Boolean) {
    Card(elevation = 4.dp,
         modifier = Modifier
             .clickable {
                 GlobalState.screenStack.push(Screen.AssignmentDetail(serviceId, assignment))
             }
             .fillMaxWidth()
             .padding(top = smallPadding, bottom = smallPadding)) {
        Column(Modifier.padding(smallPadding)) {
            Row(Modifier.fillMaxWidth()) {
                val maxPts = assignment.deadlines
                    .map { it.pointsMax }
                    .maxOrNull()
                    ?.let { prettyDouble(it) } ?: ""
                val pts =
                    assignment.pointsGained?.let { prettyDouble(it) }
                        ?: if(maxPts.isNotEmpty()) "0" else ""

                Text(assignment.name,
                     modifier = Modifier.weight(5f))
                Text(if(pts.isEmpty() && maxPts.isEmpty()) ""
                     else "$pts/$maxPts",
                     style = typography.subtitle1,
                     modifier = Modifier.weight(1f))
            }
            val firstDeadline = assignment.deadlines
                .map { it.dateTime }
                .maxOrNull()
            if(firstDeadline != null) {
                Text("Deadline: " +
                             firstDeadline.format(
                                 DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)),
                     style = typography.subtitle1)
            }
        }
    }
}
