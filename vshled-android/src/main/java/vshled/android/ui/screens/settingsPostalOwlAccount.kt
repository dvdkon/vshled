// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import libvsdata.PostalOwlClient
import vshled.android.*
import vshled.android.ui.*

@Composable
fun SettingsPostalOwlAccountHeader() {
    Text("Postal Owl Account")
}

@Composable
fun SettingsPostalOwlAccountContent(accountId: Long?) {
    Column(Modifier.verticalScroll(rememberScrollState())) {
        AccountLogin(
            AccountService.PostalOwl,
            if(accountId == null) AccountLocator.NewAccount else AccountLocator.Id(accountId),
            canName = true, canUseCasCreds = true,
            loginProc = { username, password, _ ->
                val client = PostalOwlClient(username, password)
                client.login()
            }, onLogin = { id, username, password, _ ->
                val client = PostalOwlClient(username, password)
                client.login()
                GlobalState.assignmentServices[id] = client
                if(accountId == null) {
                    GlobalState.screenStack.swapTop(Screen.SettingsPostalOwlAccount(id))
                }
            })

        if(accountId != null) {
            Divider(modifier = Modifier.padding(top = mediumPadding))
            CourseMapping(accountId)
        }
        SpacerMedium()
    }
}