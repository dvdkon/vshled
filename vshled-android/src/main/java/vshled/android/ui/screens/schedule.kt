// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.outlined.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import libvsdata.sis.*
import vshled.android.*
import vshled.android.ui.*
import java.time.DayOfWeek
import java.time.format.TextStyle
import java.util.*

@Composable
fun ScheduleHeader(year: AcademicYear, semester: Semester) {
    var showDialog by remember { mutableStateOf(false) }
    if(showDialog) {
        var yearIn by remember { mutableStateOf(year.startYear.toString()) }
        var semesterIn by remember { mutableStateOf(semester) }
        AlertDialog(onDismissRequest = { showDialog = false }, text = {
            Column {
                TextField(yearIn,
                          onValueChange = { yearIn = it },
                          label = { Text("Starting year") })
                SpacerMedium()
                Row {
                    LabeledRadioButton("Winter",
                                       semesterIn == Semester.Winter,
                                       onClick = { semesterIn = Semester.Winter },
                                       modifier = Modifier.weight(1f))
                    LabeledRadioButton("Summer",
                                       semesterIn == Semester.Summer,
                                       onClick = { semesterIn = Semester.Summer },
                                       modifier = Modifier.weight(1f))
                }
            }
        }, confirmButton = {
            Button(onClick = {
                showDialog = false
                GlobalState.screenStack.swapTop(Screen.Schedule(
                    AcademicYear(yearIn.toInt()), semesterIn))
            }, enabled = yearIn.toIntOrNull() != null) {
                Text("Load")
            }
        })
    }

    val semesterAbbr = when(semester) {
        Semester.Winter -> "WS"; Semester.Summer -> "SS"
    }

    Row(modifier = Modifier.clickable {
        showDialog = true
    }) {
        Text("Schedule: $semesterAbbr ${year.startYear}/${(year.startYear + 1) % 100}")
        Icon(VshledIcons.ArrowDropDown, "Choose semester and year")
    }
}

@Composable
fun ScheduleContent(year: AcademicYear, semester: Semester) {
    WithCachedResource(
        SisStore.getSelfScheduleCached(year, semester).combine(
            SisStore.getSelfScheduleClassTypesCached(year, semester))) { (classes, classTypes) ->
        val scrollState = rememberScrollState()
        Column(modifier = Modifier.verticalScroll(scrollState)) {
            // This isn't just a for loop with continue, since that triggers a compiler error
            (1..7).mapNotNull { dayNum ->
                val dayClasses = classes
                    .filter { it.weekday == dayNum }
                    .sortedBy { it.startTime }
                if(dayClasses.isEmpty()) null else Pair(dayNum, dayClasses)
            }.forEach { (dayNum, dayClasses) ->
                Column {
                    Text(DayOfWeek.of(dayNum)
                             .getDisplayName(TextStyle.FULL, Locale.getDefault()),
                         textAlign = TextAlign.Center,
                         fontWeight = FontWeight.Bold,
                         modifier = Modifier
                             .padding(smallPadding)
                             .fillMaxWidth())

                    for(cls in dayClasses) {
                        // Might be null due to caching problems or SIS weirdness.
                        // Ideally we'd report it, but just crashing here is bad
                        val type = classTypes[cls.id] ?: ClassType.Seminar
                        Class(year, semester, cls, type)
                    }
                }
            }
        }
    }
}

@Composable
fun Class(year: AcademicYear, semester: Semester, cls: ScheduledClass, type: ClassType) {
    Card(elevation = 4.dp,
         backgroundColor = if(type == ClassType.Lecture) primaryRedLight else secondaryAmberLight,
         modifier = Modifier
             .clickable {
                 GlobalState.screenStack.push(
                     Screen.ScheduledClass(year, semester, cls.subjectId, cls.id))
             }
             .fillMaxWidth()
             .padding(smallPadding)
    ) {
        Column(Modifier.padding(smallPadding)) {
            Row(horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()) {
                Text(cls.subjectName,
                     maxLines = 1,
                     overflow = TextOverflow.Ellipsis,
                     modifier = Modifier.weight(5f))
                Text(cls.startTime.toString(),
                     color = greyText,
                     textAlign = TextAlign.End,
                     modifier = Modifier.weight(1f))
            }
            Row(horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()) {
                Text(cls.room ?: "",
                     color = greyText,
                     modifier = Modifier.weight(1f))
                Text(when(cls.scheduledWeeks) {
                         ScheduledWeeks.AllWeeks -> ""
                         ScheduledWeeks.EvenWeeks -> "Even weeks"
                         ScheduledWeeks.OddWeeks -> "Odd weeks"
                         ScheduledWeeks.Irregular -> "Irregular"
                     },
                     color = greyText,
                     textAlign = TextAlign.Center,
                     modifier = Modifier.weight(1f))
                if(cls.duration != null && cls.startTime != null) {
                    Text((cls.startTime!! + cls.duration).toString(),
                         color = greyText,
                         textAlign = TextAlign.End,
                         modifier = Modifier.weight(1f))
                }
            }
        }
    }
}