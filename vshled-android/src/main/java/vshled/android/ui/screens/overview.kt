package vshled.android.ui.screens

import android.provider.Settings
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import vshled.android.ui.*
import java.time.LocalDate

@Composable
fun OverviewHeader() {
    Text("Overview")
}

@Composable
fun OverviewContent() {
    Column(Modifier.verticalScroll(rememberScrollState())) {
        if(GlobalState.tryGetSisAccount() == null) {
            Card(elevation = 4.dp,
                 modifier = Modifier.padding(smallPadding)) {
                Column(modifier = Modifier.padding(smallPadding)) {
                    Text("You're not logged in to SIS")
                    Text("To log in, go to Settings and set up your accounts")
                }
            }
        } else {
            val today = LocalDate.now()
            WithCachedResource(dailyScheduleForDay(today)) { (year, semester, data) ->
                SpacerSmall()
                Text("Today's schedule",
                     style = typography.h3,
                     modifier = Modifier.padding(screenPadding))
                SpacerSmall()
                val (allClasses, classTypes) = data
                val classes = allClasses.filter { it.day == today }
                if(classes.isEmpty()) {
                    Text("No classes today!", modifier = Modifier.padding(screenPadding))
                } else {
                    for(cls in classes) {
                        DailyScheduleClassCard(year, semester, classTypes, cls)
                    }
                }
            }
        }
    }
}