// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

val white = Color(0xFFFFFFFF)
val black = Color(0xFF000000)
val primaryRed = Color(0xFFD6092F)
val variantRed = Color(0xFFF14350)
val primaryRedLight = Color(0xFFFFEAEE)
val secondaryAmber = Color(0xFFFDA326)
val secondaryVariantAmber = Color(0xFFF58300)
val secondaryAmberLight = Color(0xFFFFF3E0)
val errorRed = Color(0xFFB00020)
val greyText = Color(0xFF777777)
val greyBg = Color(0xFFDDDDDD)

private val lightColorPalette = lightColors(
    primary = primaryRed,
    primaryVariant = variantRed,
    secondary = secondaryAmber,
    error = errorRed,
    background = white,
)

val typography = Typography(
    h1 = TextStyle(fontSize = 32.sp),
    h2 = TextStyle(fontSize = 24.sp, fontWeight = FontWeight.Bold),
    h3 = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold, color = secondaryAmber),
    subtitle1 = TextStyle(fontSize = 14.sp, color = greyText)
)

val shapes = Shapes(
    small = RoundedCornerShape(4.dp),
    medium = RoundedCornerShape(4.dp),
    large = RoundedCornerShape(0.dp)
)

@Composable
fun VshledTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = lightColorPalette

    MaterialTheme(
        colors = colors,
        typography = typography,
        shapes = shapes,
        content = content
    )
}

val VshledIcons = Icons.Outlined

val tinyPadding = 4.dp
val smallPadding = 8.dp
val mediumPadding = 16.dp
val largePadding = 32.dp

val screenPadding = PaddingValues(start = mediumPadding, end = mediumPadding)

val tinyFont = 9.sp
val smallFont = 14.sp
val largeFont = 24.sp

@Composable
fun SpacerSmall() = Spacer(modifier = Modifier.height(smallPadding))

@Composable
fun SpacerMedium() = Spacer(modifier = Modifier.height(mediumPadding))

@Composable
fun SpacerLarge() = Spacer(modifier = Modifier.height(largePadding))

@Composable
fun FieldLabel(text: String) = Text(text = text, style = typography.subtitle1)
