// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui

import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.jakewharton.disklrucache.DiskLruCache
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import libvsdata.*
import libvsdata.sis.*
import vshled.AccountsWithResolvedCreds
import vshled.android.AccountService
import vshled.android.Database
import vshled.android.SisStore
import vshled.android.ui.screens.*
import java.time.LocalDate
import java.time.LocalDateTime

sealed class Screen(val header: @Composable () -> Unit, val content: @Composable () -> Unit) {
    object Settings : Screen({ SettingsHeader() }, { SettingsContent() })

    object SettingsUkCasAccount : Screen(
        { SettingsUkCasAccountHeader() },
        { SettingsUkCasAccountContent() })

    object SettingsUkSisAccount : Screen(
        { SettingsUkSisAccountHeader() },
        { SettingsUkSisAccountContent() })

    object SettingsAssignmentAccounts : Screen(
        { SettingsAssignmentAccountsHeader() },
        { SettingsAssignmentAccountsContent() })

    class SettingsPostalOwlAccount(val accountId: Long?) : Screen(
        { SettingsPostalOwlAccountHeader() },
        { SettingsPostalOwlAccountContent(accountId) })

    class SettingsRecodexAccount(val accountId: Long?) : Screen(
        { SettingsRecodexAccountHeader() },
        { SettingsRecodexAccountContent(accountId) })

    class SettingsMoodleAccount(val accountId: Long?) : Screen(
        { SettingsMoodleAccountHeader() },
        { SettingsMoodleAccountContent(accountId) })

    object Overview : Screen({ OverviewHeader() }, { OverviewContent() })

    object UserInfo : Screen({ SisUserInfoHeader() }, { SisUserInfoContent() })

    class Schedule(val year: AcademicYear, val semester: Semester) : Screen(
        { ScheduleHeader(year, semester) },
        { ScheduleContent(year, semester) })

    class DailySchedule(val dayInWeek: LocalDate) : Screen(
        { DailyScheduleHeader(dayInWeek) },
        { DailyScheduleContent(dayInWeek) })

    class ScheduledClass(
        val year: AcademicYear,
        val semester: Semester,
        val subjectId: String,
        val scheduledClassId: String,
    ) : Screen(
        { ScheduledClassHeader(scheduledClassId) },
        { ScheduledClassContent(year, semester, subjectId, scheduledClassId) })

    class Assignments(val grouping: Grouping, val showHidden: Boolean) :
        Screen({ AssignmentsHeader(grouping, showHidden) },
               { AssignmentsContent(grouping, showHidden) }) {
        enum class Grouping {
            ByService,
            ByCourse,
            All
        }
    }

    class AssignmentDetail(val serviceId: Long, val assignment: Assignment) : Screen(
        { AssignmentDetailHeader() },
        { AssignmentDetailContent(serviceId, assignment) })
}

class ScreenStack(val default: Screen) {
    var stack by mutableStateOf(listOf(default))

    fun top() = stack.last()
    fun push(s: Screen) {
        stack = stack + s
    }

    fun pop() {
        stack = stack.dropLast(1)
    }

    fun swapTop(newTop: Screen) {
        stack = stack.dropLast(1) + newTop
    }

    fun clear(newBottom: Screen) {
        stack = listOf(newBottom)
    }
}

object GlobalState {
    // I'm doing everything outside of the usual Activity/Fragment... paradigm, might as well
    // make this proper global
    var activity: AppCompatActivity? = null
    var applicationContext: Context? = null
    var diskLruCache: DiskLruCache? = null
    var database: Database? = null

    var screenStack = ScreenStack(Screen.Overview)

    var sisClient by mutableStateOf(null as SisClient?)
    var sisLoggedIn by mutableStateOf(false)

    // Map of account database IDs to clients
    var assignmentServices = mutableMapOf<Long, AssignmentServiceClient>()

    fun sisIsLoggedIn(username: String) =
        try {
            val userInfo = getSelfUserInfo(sisClient!!)
            userInfo.login!!.lowercase() == username
        } catch(e: NotAuthenticatedException) {
            false
        }

    fun loginSis(username: String, password: String) {
        sisClient = SisClient(SisClient.Authentication.Cas(username, password))
        loadSisTokens(sisClient!!)

        if(!sisIsLoggedIn(username)) {
            sisClient!!.login()
            saveSisTokens(sisClient!!)
        }
        sisLoggedIn = true
    }

    fun tryGetSisAccount(): AccountsWithResolvedCreds? =
        database!!.accountQueries
            .selectByService(AccountService.UkSis.toString())
            .executeAsList().singleOrNull()

    fun tryLoginSisDbCreds() {
        val account = tryGetSisAccount() ?: return
        try {
            loginSis(account.username!!, account.password!!)
        } catch(e: Exception) {
            activity!!.runOnUiThread {
                Toast
                    .makeText(
                        applicationContext,
                        "Failed logging into SIS: " + e.message,
                        Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    const val SIS_SHARED_PREFS_NAME = "VSHLED_SIS_UK_CAS"
    const val SIS_PREF_ID_PARAM = "ID_PARAM"
    const val SIS_PREF_TID_PARAM = "TID_PARAM"
    const val SIS_PREF_COOKIES = "COOKIES"

    fun sisSharedPrefs(): SharedPreferences =
        applicationContext!!.getSharedPreferences(
            SIS_SHARED_PREFS_NAME, Context.MODE_PRIVATE)

    fun saveSisTokens(sisClient: SisClient) {
        with(sisSharedPrefs().edit()) {
            putString(SIS_PREF_ID_PARAM, sisClient.idParam)
            putString(SIS_PREF_TID_PARAM, sisClient.tidParam)
            putStringSet(SIS_PREF_COOKIES,
                         sisClient.cookies.map { it.key + "=" + it.value }.toSet())
            apply()
        }
    }

    fun loadSisTokens(sisClient: SisClient) {
        val prefs = sisSharedPrefs()
        sisClient.idParam = prefs.getString(SIS_PREF_ID_PARAM, "")
        sisClient.tidParam = prefs.getString(SIS_PREF_TID_PARAM, "")
        sisClient.cookies =
            prefs.getStringSet(SIS_PREF_COOKIES, setOf())
                ?.map {
                    val s = it.split("=")
                    Pair(s[0], s[1])
                }?.toMap() ?: mapOf()
    }

    // Without tokens for now
    fun tryLoginAssignmentServices() {
        fun reportLoginFailure(account: AccountsWithResolvedCreds, e: Exception) {
            activity!!.runOnUiThread {
                Toast
                    .makeText(
                        applicationContext,
                        "Failed logging into ${account.name ?: account.id}: ${e.message}",
                        Toast.LENGTH_LONG)
                    .show()
            }
        }

        GlobalScope.launch {
            for(account in database!!.accountQueries
                .selectByService(AccountService.PostalOwl.toString())
                .executeAsList()) {
                val client = PostalOwlClient(account.username!!, account.password!!)
                try {
                    client.login()
                    assignmentServices[account.id] = client
                } catch(e: Exception) {
                    reportLoginFailure(account, e)
                }
            }

            for(account in database!!.accountQueries
                .selectByService(AccountService.Recodex.toString())
                .executeAsList()) {
                val client = RecodexClient(RecodexClient.Authentication.Cas(
                    account.username!!, account.password!!))
                try {
                    client.login()
                    assignmentServices[account.id] = client
                } catch(e: Exception) {
                    reportLoginFailure(account, e)
                }
            }

            for(account in database!!.accountQueries
                .selectByService(AccountService.Moodle.toString())
                .executeAsList()) {
                val client = MoodleClient(account.extra!!, account.username!!, account.password!!)
                try {
                    client.login()
                    assignmentServices[account.id] = client
                } catch(e: Exception) {
                    reportLoginFailure(account, e)
                }
            }
        }
    }

    fun currentYearAndSemester(): Pair<AcademicYear, Semester> {
        val now = LocalDateTime.now()
        val startDate =
            SisStore.getSemesterStartDatesCached()(false).value
                .sortedBy { it.date }
                .findLast { it.date < now }
        return Pair(startDate!!.academicYear, startDate.semester)
    }
}
