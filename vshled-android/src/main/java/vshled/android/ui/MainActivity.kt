// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui

import android.app.ApplicationErrorReport
import android.content.Intent
import android.os.Bundle
import android.os.Debug
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import com.jakewharton.disklrucache.DiskLruCache
import com.squareup.sqldelight.android.AndroidSqliteDriver
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import libvsdata.sis.AcademicYear
import libvsdata.sis.Semester
import vshled.android.Database
import java.time.LocalDate
import java.time.Month
import java.time.YearMonth
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() {
    @DelicateCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(!Debug.isDebuggerConnected()) {
            Thread.setDefaultUncaughtExceptionHandler { thread, e ->
                val crash = ApplicationErrorReport.CrashInfo()
                crash.exceptionClassName = e.javaClass.name
                crash.exceptionMessage = e.message
                crash.stackTrace = e.stackTraceToString()

                val report = ApplicationErrorReport()
                report.packageName = application.packageName
                report.type = ApplicationErrorReport.TYPE_CRASH
                report.crashInfo = crash

                val intent = Intent(this, CrashActivity::class.java)
                intent.putExtra(Intent.EXTRA_BUG_REPORT, report)
                startActivity(intent)
            }
        }

        GlobalState.activity = this
        GlobalState.applicationContext = applicationContext

        GlobalState.diskLruCache = DiskLruCache.open(
            filesDir.resolve("cache"), 1, 1, 10_000_000)

        val dbDriver = AndroidSqliteDriver(Database.Schema, applicationContext, "user.db")
        GlobalState.database = Database(dbDriver)

        GlobalScope.launch {
            GlobalState.tryLoginSisDbCreds()
            GlobalState.tryLoginAssignmentServices()
        }

        setContent {
            VshledTheme {
                Surface(color = MaterialTheme.colors.background) {
                    MainView()
                }
            }
        }
    }

    override fun onBackPressed() {
        if(GlobalState.screenStack.stack.size <= 1) {
            super.onBackPressed()
        } else {
            GlobalState.screenStack.pop()
        }
    }
}

@Composable
fun DrawerLink(scaffoldState: ScaffoldState, icon: ImageVector, label: String, screen: Screen) {
    val scope = rememberCoroutineScope()
    Row(modifier = Modifier
        .clickable {
            GlobalState.screenStack.clear(screen)
            scope.launch { scaffoldState.drawerState.close() }
        }
        .fillMaxWidth()
        .padding(mediumPadding)) {
        Icon(icon, label, modifier = Modifier.padding(end = mediumPadding))
        Text(label)
    }
}

@Composable
fun MainView(scaffoldState: ScaffoldState = rememberScaffoldState()) {
    val scope = rememberCoroutineScope()
    Scaffold(scaffoldState = scaffoldState,
             topBar = {
                 TopAppBar(title = GlobalState.screenStack.top().header,
                           navigationIcon = {
                               Icon(VshledIcons.Menu, "Open menu",
                                    modifier = Modifier
                                        .clickable {
                                            scope.launch {
                                                scaffoldState.drawerState.open()
                                            }
                                        }
                                        .padding(14.dp))
                           })
             },
             drawerContent = {
                 Column {
                     DrawerLink(scaffoldState, VshledIcons.Settings, "Settings", Screen.Settings)
                     DrawerLink(scaffoldState, VshledIcons.Dashboard, "Overview", Screen.Overview)
                     if(GlobalState.sisClient != null) {
                         WithResource({ GlobalState.currentYearAndSemester() }) { (currentYear, currentSemester) ->
                             DrawerLink(scaffoldState,
                                        VshledIcons.AccountCircle,
                                        "User Info",
                                        Screen.UserInfo)
                             DrawerLink(scaffoldState,
                                        VshledIcons.Schedule,
                                        "Schedule",
                                        Screen.Schedule(currentYear, currentSemester))
                             DrawerLink(scaffoldState,
                                        VshledIcons.CalendarViewWeek,
                                        "Daily Schedule",
                                        Screen.DailySchedule(LocalDate.now()))
                         }
                     }
                     DrawerLink(scaffoldState,
                                VshledIcons.Assignment,
                                "Assignments",
                                Screen.Assignments(Screen.Assignments.Grouping.ByService, false))
                 }
             }) {
        Box(Modifier.padding(it)) {
            GlobalState.screenStack.top().content()
        }
    }
}