// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.outlined.Refresh
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.*
import vshled.android.*
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import kotlin.Exception
import kotlin.coroutines.cancellation.CancellationException
import kotlin.math.floor

class Recomputable<T>(val state: MutableState<T>, val calculation: () -> T) {
    val value get() = state.value
    fun recompute() {
        state.value = calculation()
    }
}

@Composable
fun <T> rememberRecomputable(calculation: @DisallowComposableCalls () -> T) =
    Recomputable(remember { mutableStateOf(calculation()) }, calculation)

@Composable
fun <T> rememberRecomputable(vararg keys: Any?, calculation: @DisallowComposableCalls () -> T) =
    Recomputable(remember(*keys) { mutableStateOf(calculation()) }, calculation)

// If I try to do the same with rememberSaveable, I keep getting
// "pending composition has not been applied", so no saveable versions for now :(
// TODO: Report this bug or fix it?

// I've also found a weirdly simple compiler crash:
//     class Wrapper(f: () -> Unit)
//     rememberSaveable { Wrapper {} }

// XXX: I'm using my own class since kotlin.Result seems to be broken, since using it gives me
// "kotlin.Result$Failure cannot be cast to kotlin.Result" on runtime
sealed class LoadingResource<T> {
    class Loading<T> : LoadingResource<T>()
    class TimedOut<T> : LoadingResource<T>()
    class Error<T>(val exception: Exception) : LoadingResource<T>()
    class Loaded<T>(val value: T) : LoadingResource<T>()
}

@Composable
fun LoadingIndicator() {
    Column(modifier = Modifier.fillMaxSize(),
           horizontalAlignment = Alignment.CenterHorizontally,
           verticalArrangement = Arrangement.Center) {
        SpacerSmall()
        Text("Loading...", fontSize = 32.sp, color = greyText)
        SpacerLarge()
        CircularProgressIndicator()
        SpacerLarge()
    }
}

@Composable
fun <T> WithResource(resGetter: () -> T, content: @Composable (T) -> Unit) =
    WithResourceKeyed(resGetter, arrayOf(), content)

@Composable
fun <T> WithResourceKeyed(resGetter: () -> T, keys: Array<Any?>, content: @Composable (T) -> Unit) {
    var res by remember {
        mutableStateOf(
            LoadingResource.Loading<T>() as LoadingResource<T>)
    }
    val scope = rememberCoroutineScope()

    var timeout by remember { mutableStateOf(5000L) } // TODO: Configure initial timeout

    fun tryGet() {
        var timeoutJob: Job? = null
        val mainJob = GlobalScope.launch {
            res = LoadingResource.Loading()
            res =
                try {
                    val v = LoadingResource.Loaded(resGetter())
                    // So that timeout can take effect without being overwritten by a
                    // late loaded resource
                    yield()
                    v
                } catch(e: CancellationException) { // Don't display timeout as an error
                    LoadingResource.TimedOut()
                } catch(e: Exception) {
                    LoadingResource.Error(e)
                } finally {
                    timeoutJob?.cancel()
                }
        }
        timeoutJob = GlobalScope.launch {
            delay(timeout)
            if(mainJob.isActive) {
                mainJob.cancel()
                res = LoadingResource.TimedOut()
            }
        }
    }

    LaunchedEffect(resGetter, *keys) {
        tryGet()
    }

    when(val resVal = res) {
        is LoadingResource.Loading -> LoadingIndicator()
        is LoadingResource.Loaded -> content(resVal.value)
        is LoadingResource.Error -> {
            var showDetails by rememberSaveable { mutableStateOf(false) }
            Column(modifier =
                   Modifier
                       .fillMaxSize()
                       .padding(smallPadding),
                   horizontalAlignment = Alignment.CenterHorizontally,
                   verticalArrangement = Arrangement.Center) {
                Text("Loading failed!", fontSize = largeFont, color = errorRed)
                SpacerLarge()
                Button(onClick = { scope.launch { tryGet() } }) {
                    Text("Retry")
                }
                SpacerLarge()
                if(showDetails) {
                    Text(resVal.exception.stackTraceToString(),
                         color = greyText,
                         fontSize = tinyFont)
                } else {
                    Text("Show details",
                         color = greyText,
                         modifier = Modifier.clickable { showDetails = true })
                }
            }
        }
        is LoadingResource.TimedOut ->
            Column(modifier = Modifier.fillMaxSize(),
                   horizontalAlignment = Alignment.CenterHorizontally,
                   verticalArrangement = Arrangement.Center) {
                Text("Loading timed out!", fontSize = largeFont, color = errorRed)
                SpacerLarge()
                Row(modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly) {
                    Button(onClick = { scope.launch { tryGet() } }) {
                        Text("Retry")
                    }
                    Button(onClick = { timeout += 5000; scope.launch { tryGet() } }) {
                        Text("Increase timeout")
                    }
                }
            }
    }
}

@Composable
fun <T> WithCachedResource(ptr: (Boolean) -> CachedValue<T>, content: @Composable (T) -> Unit) {
    var invalidate by remember { mutableStateOf(false) }
    WithResourceKeyed(
        {
            val v = ptr(invalidate)
            invalidate = false
            v
        }, arrayOf(invalidate)) { cachedValue ->
            Column {
                Row(horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier
                        .background(greyBg)
                        .padding(mediumPadding)
                        .fillMaxWidth()) {
                    val formatter = DateTimeFormatter
                        .ofLocalizedDateTime(FormatStyle.SHORT)
                        .withZone(ZoneId.systemDefault())
                    Text("Last updated: ${formatter.format(cachedValue.stored)}")
                    Icon(VshledIcons.Refresh, "Refresh",
                         modifier = Modifier.clickable { invalidate = true })
                }

                content(cachedValue.value)
            }
        }
}

@Composable
fun LabeledRadioButton(
    label: String,
    checked: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Row(modifier = modifier) {
        RadioButton(checked, onClick = onClick)
        Text(label,
             Modifier
                 .clickable { onClick() }
                 .align(Alignment.CenterVertically)
                 .padding(start = 4.dp))
    }
}

@Composable
fun LabeledCheckbox(
    label: String,
    checked: Boolean,
    onCheckedChange: (Boolean) -> Unit,
    modifier: Modifier = Modifier,
) {
    Row(modifier = modifier) {
        Checkbox(checked, onCheckedChange = onCheckedChange)
        Text(label,
             Modifier
                 .clickable { onCheckedChange(!checked) }
                 .align(Alignment.CenterVertically)
                 .padding(start = 4.dp))
    }
}

@Composable
fun LabeledValue(label: String, value: String?) {
    if(value != null) {
        SpacerMedium()
        FieldLabel(text = label)
        Text(value)
    }
}

fun prettyDouble(f: Double) =
    if(floor(f) == f) f.toInt().toString()
    else String.format("%.1f", f)

