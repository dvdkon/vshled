// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android.ui

import android.app.ApplicationErrorReport
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

class CrashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        @Suppress("DEPRECATION") // Alternative requires newer target SDK
        val report = intent.extras!![Intent.EXTRA_BUG_REPORT] as ApplicationErrorReport

        setContent {
            VshledTheme {
                Surface(color = MaterialTheme.colors.background) {
                    CrashScreen(report.crashInfo.stackTrace)
                }
            }
        }
    }
}

@Composable
fun CrashScreen(stackTrace: String) {
    var showDetails by rememberSaveable { mutableStateOf(false) }

    Column(modifier =
           Modifier
               .fillMaxSize()
               .padding(smallPadding)
               .verticalScroll(rememberScrollState()),
           horizontalAlignment = Alignment.CenterHorizontally,
           verticalArrangement = Arrangement.Center) {
        Text("App crashed!", fontSize = largeFont, color = errorRed)
        SpacerSmall()
        if(showDetails) {
            Text(stackTrace,
                 color = greyText,
                 fontSize = tinyFont)
        } else {
            Text("Show details",
                 color = greyText,
                 modifier = Modifier.clickable { showDetails = true })
        }
        SpacerLarge()
        // TODO: Report button
    }
}