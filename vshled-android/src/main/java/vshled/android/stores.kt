// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
@file:UseSerializers(InstantAsUnixTimeSerializer::class)

package vshled.android

import kotlinx.serialization.UseSerializers
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libvsdata.*
import libvsdata.sis.*
import vshled.android.ui.GlobalState
import java.io.DataInputStream
import java.io.DataOutputStream
import java.security.MessageDigest
import java.time.Duration
import java.time.Instant

val CacheJson = Json { allowStructuredMapKeys = true }

class CachedValue<T>(val stored: Instant, val value: T)

inline fun <reified T> getCachedValuePtr(
    key: String,
    softInvalidateAfter: Duration?,
    crossinline getter: () -> T,
) =
    { invalidate: Boolean ->
        getCachedValue(key, getter, softInvalidateAfter, invalidate = invalidate)
    }

fun encodeCacheKey(str: String): String {
    // Due to length limitations we had to switch to less readable hashing
    val sha256Digest = MessageDigest.getInstance("SHA-256")
    val hash = sha256Digest.digest(str.toByteArray())
    return hash.joinToString("") { "%02x".format(it) }
}

inline fun <reified T> getCachedValueOrNull(key: String): CachedValue<T>? {
    val encKey = encodeCacheKey(key)
    val snapshot = GlobalState.diskLruCache!!.get(encKey)
    return try {
        val inStream = DataInputStream(snapshot.getInputStream(0))
        val stored = Instant.ofEpochSecond(inStream.readLong())
        val value: T = CacheJson.decodeFromString(inStream.readUTF())
        inStream.close()
        CachedValue(stored, value)
    } catch(e: Exception) {
        null
    }
}

// Cache invalidation is a tricky thing: It should definitely happen at some point,
// but one benefit of caching everything is being resilient to outside service outages
// or changes. That doesn't mesh with suddenly deleting something from cache because
// it's too old. What if some service is just temporarily broken? So we have two age thresholds:
// when the soft threshold is crossed, every cache access will try to refresh that item.
// If it fails, we just get the cached version as usual. (XXX: Timeouts will probably break this)
// Then there's the hard threshold. If that is crossed, something's probably very wrong
// (assuming regular usage) and the user should know. At that point, the cached value won't
// be used.

val cacheEditorLock = Any()

inline fun <reified T> saveCachedValue(key: String, cachedValue: CachedValue<T>) {
    synchronized(cacheEditorLock) {
        val editor = GlobalState.diskLruCache!!.edit(key)
        val outStream = DataOutputStream(editor.newOutputStream(0))
        // We aren't serializing the CachedValue directly, because that triggers
        // some weird bug
        // TODO: Fix the root issue!
        outStream.writeLong(cachedValue.stored.epochSecond)
        outStream.writeUTF(CacheJson.encodeToString(cachedValue.value))
        outStream.close()
        editor.commit()
    }
}

inline fun <reified T> getCachedValue(
    key: String, getter: () -> T,
    softInvalidateAfter: Duration?,
    hardInvalidateAfter: Duration? = softInvalidateAfter?.let { it + Duration.ofDays(7) },
    invalidate: Boolean = false,
): CachedValue<T> {
    val valueFromCache = if(invalidate) null else getCachedValueOrNull<T>(key)
    if(valueFromCache != null &&
        (softInvalidateAfter == null ||
                valueFromCache.stored + softInvalidateAfter > Instant.now()))
        return valueFromCache

    val newValue = try {
        getter()
    } catch(e: Exception) {
        if(valueFromCache != null &&
            (hardInvalidateAfter == null ||
                    valueFromCache.stored + hardInvalidateAfter > Instant.now()))
            return valueFromCache
        else
            throw e
    }

    val encKey = encodeCacheKey(key)
    val cachedValue = CachedValue(Instant.now(), newValue)
    saveCachedValue(encKey, cachedValue)
    return cachedValue
}

// See, I told you (kinda) monads are useful!
fun <A, B> ((Boolean) -> CachedValue<A>).bind(f: (A) -> (Boolean) -> CachedValue<B>) =
    { invalidate: Boolean ->
        val cv1 = this(invalidate)
        val cv2 = f(cv1.value)(invalidate)
        CachedValue(minOf(cv1.stored, cv2.stored), cv2.value)
    }

fun <A, B> ((Boolean) -> CachedValue<A>).combine(ptr2: (Boolean) -> CachedValue<B>) =
    { invalidate: Boolean ->
        val cv1 = this(invalidate)
        val cv2 = ptr2(invalidate)
        CachedValue(minOf(cv1.stored, cv2.stored), Pair(cv1.value, cv2.value))
    }

fun <A, B> ((Boolean) -> CachedValue<A>).map(f: (A) -> B) =
    { invalidate: Boolean ->
        val cv = this(invalidate)
        CachedValue(cv.stored, f(cv.value))
    }

fun <T> combineCachedValuePtrList(ptrs: List<(Boolean) -> CachedValue<T>>) =
    { invalidate: Boolean ->
        val cvs = ptrs.map { it(invalidate) }
        CachedValue(cvs.minOf { it.stored }, cvs.map { it.value })
    }

// Will run the function, if it fails with NotAuthenticatedException, it will call
// client.login() and retry once
fun <T> loginRetry(client: Any, f: () -> T) =
    if(client is AuthenticatedClient) {
        try {
            f()
        } catch(_: NotAuthenticatedException) {
            client.login()
            f()
        }
    } else {
        f()
    }

object SisStore {
    val client: SisClient get() = GlobalState.sisClient!!

    fun getSelfUserInfoCached() =
        getCachedValuePtr("sis-self-user-data", Duration.ofDays(365)) {
            loginRetry(client) { getSelfUserInfo(client) }
        }

    fun getSelfScheduleCached(year: AcademicYear, semester: Semester) =
        getCachedValuePtr(
            "sis-self-schedule-${year.startYear}-$semester", null) {
            loginRetry(client) { getSelfSchedule(client, year, semester) }
        }

    fun getSelfDailyScheduleCached(year: AcademicYear, semester: Semester) =
        getCachedValuePtr(
            "sis-self-schedule-${year.startYear}-$semester", null) {
            loginRetry(client) { getSelfDailySchedule(client, year, semester) }
        }

    fun getSelfScheduleClassTypesCached(year: AcademicYear, semester: Semester) =
        getCachedValuePtr("sis-self-schedule-class-types-${year.startYear}-$semester", null) {
            loginRetry(client) { getSelfScheduleClassTypes(client, year, semester) }
        }

    fun getSubjectCached(subjectId: String) =
        getCachedValuePtr("sis-subject-$subjectId", Duration.ofDays(365)) {
            loginRetry(client) { getSubject(client, subjectId) }
        }

    fun getScheduledClassDetailsCached(
        year: AcademicYear,
        semester: Semester,
        facultyId: String,
        scheduledClassId: String,
    ) =
        getCachedValuePtr("sis-scheduled-class-details-${year.startYear}-$semester-$facultyId-$scheduledClassId",
                          null) {
            loginRetry(client) {
                getScheduledClassDetails(client, year, semester, facultyId, scheduledClassId)
            }
        }

    fun getSemesterStartDatesCached() =
        getCachedValuePtr("sis-semester-start-dates", Duration.ofDays(100)) {
            loginRetry(client) {
                getSemesterStartDates(client)
            }
        }
}

object AssignmentStore {
    fun client(id: Long) = GlobalState.assignmentServices[id]!!

    fun getCoursesCached(clientId: Long) =
        getCachedValuePtr("assignments-$clientId-courses", Duration.ofDays(100)) {
            loginRetry(client(clientId)) { client(clientId).getCourses() }
        }

    fun getAssignmentsCached(clientId: Long, courseId: String) =
        getCachedValuePtr("assignments-$clientId-assignments-$courseId", Duration.ofDays(3)) {
            loginRetry(client(clientId)) { client(clientId).getAssignments(courseId) }
        }

    fun getAssignmentPostsCached(clientId: Long, assignmentId: String) =
        getCachedValuePtr("assignments-$clientId-assignment-posts-$assignmentId",
                          Duration.ofDays(1)) {
            loginRetry(client(clientId)) { client(clientId).getAssignmentPosts(assignmentId) }
        }
}