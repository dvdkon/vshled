// This file is part of VŠhled and is licenced under the GNU GPLv3 or later
// (c) 2021 David Koňařík
package vshled.android

import vshled.android.ui.GlobalState

enum class AccountService {
    UkCas,
    UkSis,
    PostalOwl,
    Recodex,
    Moodle;

    fun displayName() =
        when(this) {
            UkCas -> "CAS UK"
            UkSis -> "SIS UK"
            PostalOwl -> "Postal Owl"
            Recodex -> "Recodex"
            Moodle -> "Moodle"
        }
}

fun getCasAccount() =
    GlobalState.database!!.accountQueries
        .selectByService(AccountService.UkCas.toString())
        .executeAsList()
        .singleOrNull()
