// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
rootProject.name = "vshled"

include("libvsdata")
include("vsdata-cli")
include("vshled-android")
