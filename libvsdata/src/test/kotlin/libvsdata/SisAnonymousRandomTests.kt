// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
package libvsdata

import libvsdata.sis.SisClient
import libvsdata.sis.getLocalFaculties
import libvsdata.sis.getSubject
import libvsdata.sis.searchSubjects
import org.junit.jupiter.api.*

@Tags(Tag("sis"),
      Tag("slow"),
      Tag("anonymous"))
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SisAnonymousRandomTests {
    var client: SisClient? = null

    @Test
    @BeforeAll
    fun anonymousLogin() {
          client = SisClient(SisClient.Authentication.Anonymous)
          client!!.login()
    }

    @Test
    @Disabled // Can take minutes to complete
    fun `100 random subjects from each faculty`() {
        for(faculty in getLocalFaculties(client!!)) {
            val subjects = assertDoesNotThrow {
                searchSubjects(client!!, faculty.id, null, null)
            }
            val subjectsToTest = subjects.shuffled().take(100)
            for(subject in subjectsToTest) {
                assertDoesNotThrow("Faculty ${faculty.id}, Subject ${subject.id}") {
                    getSubject(client!!, subject.id)
                }
            }
        }
    }
}