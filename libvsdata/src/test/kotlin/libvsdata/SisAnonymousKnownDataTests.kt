// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2022 David Koňařík
package libvsdata

import libvsdata.sis.*
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Tags(Tag("sis"),
      Tag("quick"),
      Tag("anonymous"))
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SisAnonymousKnownDataTests {
    var client: SisClient? = null

    @Test
    @BeforeAll
    fun anonymousLogin() {
        client = SisClient(SisClient.Authentication.Anonymous)
        client!!.login()
    }

    @Test
    fun `list local faculties`() {
        assertListEquals(
            listOf(
                LocalFaculty(id = "11110", name = "1. lékařská fakulta", shortName = "1.LF"),
                LocalFaculty(id = "11120", name = "3. lékařská fakulta", shortName = "3.LF"),
                LocalFaculty(id = "11130", name = "2. lékařská fakulta", shortName = "2.LF"),
                LocalFaculty(id = "11140", name = "Lékařská fakulta v Plzni", shortName = "LFP"),
                LocalFaculty(id = "11150",
                             name = "Lékařská fakulta v Hradci Králové",
                             shortName = "LFHK"),
                LocalFaculty(id = "11160",
                             name = "Farmaceutická fakulta v Hradci Králové",
                             shortName = "FaF"),
                LocalFaculty(id = "11210", name = "Filozofická fakulta", shortName = "FF"),
                LocalFaculty(id = "11220", name = "Právnická fakulta", shortName = "PF"),
                LocalFaculty(id = "11230", name = "Fakulta sociálních věd", shortName = "FSV"),
                LocalFaculty(id = "11240", name = "Fakulta humanitních studií", shortName = "FHS"),
                LocalFaculty(id = "11260",
                             name = "Katolická teologická fakulta",
                             shortName = "KTF"),
                LocalFaculty(id = "11270",
                             name = "Evangelická teologická fakulta",
                             shortName = "ETF"),
                LocalFaculty(id = "11280", name = "Husitská teologická fakulta", shortName = "HTF"),
                LocalFaculty(id = "11310", name = "Přírodovědecká fakulta", shortName = "PřF"),
                LocalFaculty(id = "11320",
                             name = "Matematicko-fyzikální fakulta",
                             shortName = "MFF"),
                LocalFaculty(id = "11410", name = "Pedagogická fakulta", shortName = "PedF"),
                LocalFaculty(id = "11510",
                             name = "Fakulta tělesné výchovy a sportu",
                             shortName = "FTVS"),
                LocalFaculty(id = "11610",
                             name = "Ústav dějin Univerzity Karlovy a archiv Univerzity Karlovy",
                             shortName = "ÚDAUK"),
                LocalFaculty(id = "11620",
                             name = "Centrum pro teoretická studia",
                             shortName = "CTS"),
                LocalFaculty(id = "11640",
                             name = "Centrum pro ekonomický výzkum a doktorské studium",
                             shortName = "CERGE"),
                LocalFaculty(id = "11670",
                             name = "Ústav jazykové a odborné přípravy",
                             shortName = "ÚJOP"),
                LocalFaculty(id = "11680",
                             name = "Ústřední knihovna",
                             shortName = "ÚK"),
                LocalFaculty(id = "11690",
                             name = "Centrum pro otázky životního prostředí",
                             shortName = "COŽP"),
                LocalFaculty(id = "11710",
                             name = "Centrum pro přenos poznatků a technologií",
                             shortName = "CPPT"),
                LocalFaculty(id = "11901", name = "Rektorát Univerzity Karlovy", shortName = "RUK"),
            ).sortedBy { it.id },
            getLocalFaculties(client!!).sortedBy { it.id },
        )
    }

    @Test
    fun `search subjects`() {
        assertListEquals(
            listOf(SubjectSimple(
                id = "NMET065",
                name = "Uživatelsky přátelský Linux",
                semesters = setOf(Semester.Winter),
                semesterSpecific = mapOf(Semester.Winter to SemesterSpecificSubjectInfo(
                    lectureBlocks = 2,
                    seminarBlocks = 1,
                    requirements = CompletionRequirements.ClassifiedCredit)),
                department = "32-KFA",
                faculty = "MFF")),
            searchSubjects(client!!, "11320", "Linux", "NM", false))
    }

    @Test
    fun `get subject details (NMET065)`() {
        assertEquals(
            SubjectDetail(
                name = "Uživatelsky přátelský Linux",
                id = "NMET065",
                englishName = "User-friendly Linux",
                department = NamedWithId(name = "Katedra fyziky atmosféry (32-KFA)",
                                         id = "32-KFA"),
                faculty = NamedWithId(name = "Matematicko-fyzikální fakulta",
                                      id = "11320"),
                validity = "od 2022",
                semesters = setOf(Semester.Winter),
                credits = 4,
                semesterSpecific = mapOf(Semester.Winter to SemesterSpecificSubjectInfo(
                    lectureBlocks = 2,
                    seminarBlocks = 1,
                    requirements = CompletionRequirements.ClassifiedCredit)),
                capacity = SubjectAttendance(Attendance.Unlimited, Attendance.Unlimited),
                minStudentNum = SubjectAttendance(Attendance.Unlimited, Attendance.Unlimited),
                isTaught = false,
                language = "čeština, angličtina",
                teachingMethod = "prezenční",
                links = listOf(),
                note = null,
                guarantors = listOf(NamedWithId(name = "Mgr. Peter Huszár, Ph.D.",
                                                id = "13359")),
                classes = listOf(),
                classificationCategories = listOf(),
                prerequisites = listOf(),
                corequisites = listOf(),
                incompatibleSubjects = listOf(),
                incompatibleForSubjects = listOf(),
                replacementSubjects = listOf(),
                replacementForSubjects = listOf(),
                annotation = BilingualText(czech = "Základní principy operačního systému Linux pro úplné začátečníky s ohledem na aplikace ve fyzice. Absolvent by se měl být schopen v systému orientovat a pracovat se základními službami, především pohodlně zvládat práci v příkazové řádce. Předmět je určen všem studentům bakalářského i magisterského studia.",
                                           english = "Fundamentals of the Linux operating system including the work in command line and related applications for beginners."),
                subjectGoals = BilingualText(czech =
                                             "<p>Cílem semináře je základní orientace a práce v operačním systému Linux, seznámení se s příkazovou řádkou a naučit se pohodlné a účinné práce v ní.</p>",
                                             english = "<p>The aim of the seminar is basic orientation and work under operating system Linux.</p>"),
                completionRequirements = BilingualText(czech = "<p>Podmínkou úspěšného absolvování předmětu je získání klasifikovaného zápočtu. </p>  \n<p>Udělení zápočtu je podmíněn aktivní účast studentů na semináři, splnění domácích (alespoň 7 z 10) i zápočtového úkolu. </p>",
                                                       english = null),
                literature = BilingualText(czech = "<p>Povinná: </p> \n<p>C. Herborth: Unix a Linux - Názorný průvodce, Computer Press, Praha, 2006. </p>  \n<p>Doporučená: </p> \n<p>D. J. Barrett: Linux - Kapesní přehled, Computer Press, Praha, 2006. </p> \n<p>M. Sobell: Mistrovství v RedHat a Fedora Linux, Computer Press, Praha, 2006. </p>",
                                           english = "<p>D. J. Barrett: Linux - Kapesní přehled, Computer Press, Praha, 2006. </p> \n<p>C. Herborth: Unix a Linux - Názorný průvodce, Computer Press, Praha, 2006.</p>"),
                teachingMethods = BilingualText(czech = "<p>Výuka předmětu probíhá formou semináře prací na počítači.</p>",
                                                english = "<p>The lessons are organized as seminars by work on PC. </p>"),
                examRequirements = BilingualText(czech = "<p>Podmínkou pro udělení zápočtu je aktivní účast studentů na semináři, splnění domácích i zápočtového úkolu.</p>",
                                                 english = "<p>The credits are granted under condition of active attending of seminar and accomplishment of seminar work. </p>"),
                syllabus = BilingualText(czech = "<p>Historie, úvod a základní principy operačního systému Linux. Přihlášení do systému, textové a grafické rozhraní. Procesy, souborový systém, uživatel a skupina. Příkazová řádka, základní aplikace a příkazy. Vzdálené připojení a správa serverových služeb. </p>",
                                         english = "<p>History, introduction and basics of the Linux operating system. Login into the system, text and graphical interface. Processes, file system, users and groups. Command line, basic applications and commands. Remote login and administration of the server applications. </p>"),
                entryRequirements = BilingualText(czech = "<p>Pro tento předmět nejsou specifikovány žádné vstupní podmínky.</p>",
                                                  english = "<p>The seminar registration is not conditioned by any way.</p>"),
                statistics = mapOf()),
            getSubject(client!!, "NMET065"))
    }

    @Test
    fun `get subject details (NMMA202)`() {
        assertEquals(
            SubjectDetail(
                name = "Matematická analýza 4",
                id = "NMMA202",
                englishName = "Mathematical Analysis 4",
                department = NamedWithId(name = "Katedra matematické analýzy (32-KMA)",
                                         id = "32-KMA"),
                faculty = NamedWithId(name = "Matematicko-fyzikální fakulta",
                                      id = "11320"),
                validity = "od 2020",
                semesters = setOf(Semester.Summer),
                credits = 8,
                semesterSpecific = mapOf(Semester.Summer to SemesterSpecificSubjectInfo(
                    lectureBlocks = 4,
                    seminarBlocks = 2,
                    requirements = CompletionRequirements.FinalExamAndCredit)),
                capacity = SubjectAttendance(Attendance.Unlimited, Attendance.Unlimited),
                minStudentNum = SubjectAttendance(Attendance.Unlimited, Attendance.Unlimited),
                isTaught = false,
                language = "čeština, angličtina",
                teachingMethod = "prezenční",
                links = listOf(),
                note = null,
                guarantors = listOf(),
                classes = listOf(
                    NamedWithId(name = "M Bc. MMIB", id = "MBIB"),
                    NamedWithId(name = "M Bc. MMIB > Doporučené volitelné", id = "MBIBV"),
                    NamedWithId(name = "M Bc. MMIT", id = "MBIT"),
                    NamedWithId(name = "M Bc. MMIT > Povinně volitelné", id = "MBITPV"),
                    NamedWithId(name = "M Bc. OM", id = "MBOM"),
                    NamedWithId(name = "M Bc. OM > Povinné", id = "MBOMP"),
                    NamedWithId(name = "M Bc. OM > 2. ročník", id = "MBOM2")),
                classificationCategories = listOf(),
                prerequisites = listOf(
                    RequisiteGroup(
                        minPoints = 1,
                        subjects = listOf(
                            RequisiteSubject(
                                name = "Matematická analýza 1",
                                id = "NMMA101",
                                points = 1),
                            RequisiteSubject(
                                name = "Matematická analýza 2",
                                id = "NMMA102",
                                points = 1),
                            RequisiteSubject(
                                name = "Matematická analýza I",
                                id = "NMAF051",
                                points = 1),
                            RequisiteSubject(
                                name = "Matematická analýza II",
                                id = "NMAF052",
                                points = 1),
                            RequisiteSubject(
                                name = "Matematická analýza I",
                                id = "NOFY151",
                                points = 1),
                            RequisiteSubject(
                                name = "Matematická analýza II",
                                id = "NOFY152",
                                points = 1)))),
                corequisites = listOf(
                    RequisiteGroup(minPoints = 0,
                                   subjects = listOf(RequisiteSubject(
                                       name = "Matematická analýza 3 (pro: zápis)",
                                       id = "NMMA201",
                                       points = 0)))),
                incompatibleSubjects = listOf(
                    NamedWithId(name = "Matematická analýza 2b (pro: zápis) [nevyučován]",
                                id = "NMAA004")),
                incompatibleForSubjects = listOf(
                    NamedWithId(name = "Matematická analýza 4 (pro: zápis)",
                                id = "NMMA204")),
                replacementSubjects = listOf(
                    NamedWithId(name = "Matematická analýza 2b (pro: zápis + plnění) [nevyučován]",
                                id = "NMAA004"),
                    NamedWithId(name = "Matematická analýza 4 (pro: zápis + plnění)",
                                id = "NMMA204")),
                replacementForSubjects = listOf(
                    NamedWithId(name = "Matematická analýza 2b (pro: zápis + plnění) [nevyučován]",
                                id = "NMAA004"),
                    NamedWithId(name = "Matematická analýza 4 (pro: zápis + plnění)",
                                id = "NMMA204")),
                annotation = BilingualText(czech = "Čtvrtá část čtyřsemestrálního kursu matematické analýzy pro bakalářský obor Obecná matematika.",
                                           english = "The fourth part of a four-semester course in mathematical analysis for bachelor's program General Mathematics."),
                subjectGoals = BilingualText(czech = null, english = null),
                completionRequirements = BilingualText(czech = "<p>Podrobné požadavky k zápočtu a ke zkoušce jsou uvedeny na webové stránce přednášejícího http://www.karlin.mff.cuni.cz/~pick/</p>",
                                                       english = null),
                literature = BilingualText(czech = "<p>ZÁKLADNÍ LITERATURA </p>  \n<p>V. Jarník: Diferenciální počet II </p>  \n<p>V. Jarník: Integrální počet I,II </p>  \n<p>L. Zajíček: Vybrané partie z matematické analýzy pro 2. ročník </p>  \n<p>L. Zajíček: Vybrané úlohy z matematické analýzy pro 1. a 2. ročník </p>  \n<p>P. Holický, O.Kalenda: Metody řešení vybraných úloh z matematické analýzy pro 2. až 4. semestr </p>  \n<p>DOPLŇKOVÁ LITERATURA </p>  \n<p>B. P. Demidovič: Sbírka úloh z matematické analýzy </p>  \n<p>W. Rudin: Principles of Math. Analysis </p>  \n<p>J. Lukeš a kol.: Problémy z matematické analýzy (skriptum) </p>",
                                           english = null),
                teachingMethods = BilingualText(czech = null, english = null),
                examRequirements = BilingualText(czech = null, english = null),
                syllabus = BilingualText(czech = "<p>19. Metrické prostory III. </p>  \n<p>a) Množiny husté, řídké, první a druhé kategorie, residuální. </p>  \n<p>b) Banachova věta o kontrakci, důkaz Picardovy věty. </p>  \n<p>c) Separabilní prostory, totálně omezené prostory, kompaktní prostory. </p>  \n<p>d) Souvislé prostory. </p>  \n<p>20. Křivkový a plošný integrál (parametricky). </p>  \n<p>a) Hausdorffovy míry. </p>  \n<p>b) Křivky, plochy a jejich orientace. </p>  \n<p>c) Gaussova, Greenova a Stokesova věta. </p>  \n<p>d) Hlavní věta teorie pole. </p>  \n<p>21. Číselné řady II </p>  \n<p>a) Přerovnávání řad, Riemannova věta. </p>  \n<p>b) Cauchyův součin řad, Mertensova věta, Abelova věta. </p>  \n<p>c) Zobecněné řady. </p>  \n<p>22. Absolutně spojité funkce, funkce s konečnou variací </p>  \n<p>23. Fourierovy řady </p>  \n<p>a) Základy teorie Fourierových řad. </p>  \n<p>b) Dirichletovo a Fejérovo jádro, Cesarovská sčítatelnost, Fejérova věta. </p>  \n<p>c) Riemannovo-Lebesgueovo lemma, věta o lokalizaci, Jordanovo-Dirichletovo kritérium, Diniovo kritérium. </p>",
                                         english = null),
                entryRequirements = BilingualText(czech = null, english = null),
                statistics = mapOf()),
            getSubject(client!!, "NMMA202")
        )
    }

    @Test
    fun `subject schedule (KFIL017 2019-2020)`() {
        assertListEquals(
            listOf(
                ScheduledClass(
                    id = "19aKFIL017p1",
                    subId = "19aKFIL017p1",
                    subjectId = "KFIL017",
                    subjectName = "Otázky současné filozofie",
                    weekday = 4,
                    startTime = LocalTime.of(15, 30),
                    room = "P1",
                    duration = Duration.ofMinutes(90),
                    firstWeek = 1,
                    firstWeekOneShot = null,
                    weekCount = 14,
                    scheduledWeeks = ScheduledWeeks.AllWeeks,
                    teachers = "Sousedík Prokop, Mgr. Ing., Ph.D.")
            ),
            getSubjectSchedule(
                client!!, AcademicYear(2019), Semester.Winter, "11260", "KFIL017"))
    }

    @Test
    fun `subject daily schedule (KFIL017 2019-2020)`() {
        assertListEquals(
            listOf(
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 10, 3),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 10, 10),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 10, 17),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 10, 24),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 10, 31),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 11, 7),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 11, 14),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 11, 21),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 11, 28),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 12, 5),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 12, 12),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2019, 12, 19),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
                ScheduledClassDay(id = "19aKFIL017p1",
                                  subId = "19aKFIL017p1",
                                  subjectId = "KFIL017",
                                  subjectName = "Otázky současné filozofie",
                                  weekday = 4,
                                  day = LocalDate.of(2020, 1, 9),
                                  startTime = LocalTime.of(15, 30),
                                  room = "P1",
                                  duration = Duration.ofMinutes(90),
                                  teachers = "Sousedík Prokop, Mgr. Ing., Ph.D."),
            ),
            getSubjectDailySchedule(
                client!!, AcademicYear(2019), Semester.Winter, "11260", "KFIL017"))
    }

    @Test
    fun `subject schedule class types (B01438 2019-2020)`() {
        assertMapEquals(
            mapOf("19aB01438p1" to ClassType.Lecture, "19aB01438x01" to ClassType.Seminar),
            getSubjectScheduleClassTypes(
                client!!, AcademicYear(2019), Semester.Winter, "11110", "B01438"))
    }

    @Test
    fun `harmonogram of semesters until 2010`() {
        assertListEquals(
            listOf(
                ScheduleEvent(from = LocalDateTime.parse("2008-10-06T00:00"),
                              to = LocalDateTime.parse("2009-01-09T23:59"),
                              facultyName = "FF",
                              typeName = "Akademický rok - výuka",
                              note = "Výuka v ZS 2008/2009"),
                ScheduleEvent(from = LocalDateTime.parse("2009-02-23T00:00"),
                              to = LocalDateTime.parse("2009-05-29T23:59"),
                              facultyName = "FTVS",
                              typeName = "Akademický rok - výuka",
                              note = "Výuka - rezervace LS 2008/9"),
                ScheduleEvent(from = LocalDateTime.parse("2009-02-23T00:00"),
                              to = LocalDateTime.parse("2009-05-15T23:59"),
                              facultyName = "FF",
                              typeName = "Akademický rok - výuka",
                              note = "Výuka v LS 2008/2009"),
                ScheduleEvent(from = LocalDateTime.parse("2009-09-28T00:00"),
                              to = LocalDateTime.parse("2010-01-08T23:59"),
                              facultyName = "FHS",
                              typeName = "Akademický rok - výuka",
                              note = "Výuka v zimním semestru"),
                ScheduleEvent(from = LocalDateTime.parse("2009-09-28T00:00"),
                              to = LocalDateTime.parse("2010-01-17T23:59"),
                              facultyName = "PřF",
                              typeName = "Akademický rok - výuka",
                              note = "Zimní semestr 2009/10 - rozvrhovaná výuka"),
                ScheduleEvent(from = LocalDateTime.parse("2009-09-28T00:00"),
                              to = LocalDateTime.parse("2010-01-15T23:59"),
                              facultyName = "FTVS",
                              typeName = "Akademický rok - výuka",
                              note = "výuka 20091"),
                ScheduleEvent(from = LocalDateTime.parse("2009-09-29T00:00"),
                              to = LocalDateTime.parse("2010-01-08T23:59"),
                              facultyName = "FF",
                              typeName = "Akademický rok - výuka",
                              note = "Výuka v ZS 2009/2010"),
                ScheduleEvent(from = LocalDateTime.parse("2009-09-29T00:00"),
                              to = LocalDateTime.parse("2010-01-08T23:59"),
                              facultyName = "PedF",
                              typeName = "Akademický rok - výuka",
                              note = "2009/10 ZS výuka prezenčního studia"),
                ScheduleEvent(from = LocalDateTime.parse("2009-09-29T00:00"),
                              to = LocalDateTime.parse("2010-01-15T23:59"),
                              facultyName = "LFP",
                              typeName = "Akademický rok - výuka",
                              note = "Akademický rok - výuka zimní semestr 2009"),
                ScheduleEvent(from = LocalDateTime.parse("2009-09-29T00:00"),
                              to = LocalDateTime.parse("2010-01-24T23:59"),
                              facultyName = "UK",
                              typeName = "Akademický rok - výuka",
                              note = "Výuka v zimním semestru akadem. roku 2009/2010")),
            getYearSchedule(client!!, "VYUKA")
                .filter { it.from < LocalDateTime.of(2010, 1, 1, 0, 0) })
    }

    @Test
    fun `semester start dates until 2012`() {
        assertListEquals(
            listOf(
                SemesterStartDate(academicYear = AcademicYear(2008),
                                  semester = Semester.Winter,
                                  date = LocalDateTime.parse("2008-10-06T00:00")),
                SemesterStartDate(academicYear = AcademicYear(2008),
                                  semester = Semester.Summer,
                                  date = LocalDateTime.parse("2009-02-23T00:00")),
                SemesterStartDate(academicYear = AcademicYear(2009),
                                  semester = Semester.Winter,
                                  date = LocalDateTime.parse("2009-09-28T00:00")),
                SemesterStartDate(academicYear = AcademicYear(2009),
                                  semester = Semester.Summer,
                                  date = LocalDateTime.parse("2010-02-22T00:00")),
                SemesterStartDate(academicYear = AcademicYear(2010),
                                  semester = Semester.Winter,
                                  date = LocalDateTime.parse("2010-09-29T00:00")),
                SemesterStartDate(academicYear = AcademicYear(2010),
                                  semester = Semester.Summer,
                                  date = LocalDateTime.parse("2011-02-21T00:00")),
                SemesterStartDate(academicYear = AcademicYear(2011),
                                  semester = Semester.Winter,
                                  date = LocalDateTime.parse("2011-10-01T00:00")),
                SemesterStartDate(academicYear = AcademicYear(2011),
                                  semester = Semester.Summer,
                                  date = LocalDateTime.parse("2012-02-20T00:00")),
                SemesterStartDate(academicYear = AcademicYear(2012),
                                  semester = Semester.Winter,
                                  date = LocalDateTime.parse("2012-10-01T00:00")),
                SemesterStartDate(academicYear = AcademicYear(2012),
                                  semester = Semester.Summer,
                                  date = LocalDateTime.parse("2013-02-18T00:00"))),
            getSemesterStartDates(client!!).filter { it.academicYear.startYear <= 2012 })
    }
}