// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
package libvsdata

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.assertAll

fun <T> assertListEquals(expected: List<T>, actual: List<T>) =
    assertAll(listOf(
        listOf { assertEquals(expected.size, actual.size) },
        expected.mapIndexed { i, v -> { assertEquals(v, actual[i]) } },
    ).flatten())

fun <K, V> assertMapEquals(expected: Map<K, V>, actual: Map<K, V>) =
    assertAll(
        listOf { assertEquals(expected.size, actual.size) }
                + expected.keys.map { { assertEquals(expected[it], actual[it]) } }
    )