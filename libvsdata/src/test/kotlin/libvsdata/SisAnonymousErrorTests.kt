// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
package libvsdata

import libvsdata.sis.*
import org.junit.jupiter.api.*

// Tests to check whether a client that's supposed to be logged in but isn't is correctly detected
@Tags(Tag("sis"), Tag("quick"), Tag("anonymous"))
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SisAnonymousErrorTests {
    var client = SisClient(SisClient.Authentication.Cas("", ""))

    @Test
    fun `NotAuthenticatedException correctly thrown`() {
        assertThrows<NotAuthenticatedException> { getSelfUserInfo(client) }
        assertThrows<NotAuthenticatedException> { getSelfStudyEnrollments(client) }
        assertThrows<NotAuthenticatedException> {
            getSelfSchedule(client, AcademicYear(2020), Semester.Winter)
        }
        assertThrows<NotAuthenticatedException> {
            getSubjectSchedule(client, AcademicYear(2020), Semester.Winter, "", "")
        }
        assertThrows<NotAuthenticatedException> {
            getSelfDailySchedule(client, AcademicYear(2020), Semester.Winter)
        }
        assertThrows<NotAuthenticatedException> {
            getSubjectDailySchedule(client, AcademicYear(2020), Semester.Winter, "", "")
        }
        assertThrows<NotAuthenticatedException> {
            getSelfScheduleClassTypes(client, AcademicYear(2020), Semester.Winter)
        }
        assertThrows<NotAuthenticatedException> {
            getSubjectScheduleClassTypes(client, AcademicYear(2020), Semester.Winter, "", "")
        }
        assertThrows<NotAuthenticatedException> {
            getScheduledClassDetails(client, AcademicYear(2020), Semester.Winter, "", "")
        }
        assertThrows<NotAuthenticatedException> {
            getSubject(client, "")
        }
        assertThrows<NotAuthenticatedException> {
            getSemesterStartDates(client)
        }
    }
}