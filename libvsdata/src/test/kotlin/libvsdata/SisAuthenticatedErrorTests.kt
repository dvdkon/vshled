// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2022 David Koňařík
package libvsdata

import libvsdata.sis.SisClient
import org.junit.jupiter.api.*

@Tags(
    Tag("sis"),
    Tag("quick"),
    Tag("authenticated"))
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SisAuthenticatedErrorTests {
    var client: SisClient? = null

    @Test
    @BeforeAll
    fun anonymousLogin() {
        client = SisClient(SisClient.Authentication.Cas(
            System.getenv("LIBVSDATA_TEST_SIS_USERNAME"),
            System.getenv("LIBVSDATA_TEST_SIS_PASSWORD")))
        client!!.login()
    }
}