// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
package libvsdata

import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.helper.HttpConnection
import org.jsoup.nodes.Document
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class PostalOwlClient(val username: String, val password: String)
    : AssignmentServiceClient, AuthenticatedClient {
    val baseUrl = "https://kam.mff.cuni.cz/owl/"
    var cookies: Map<String, String> = HashMap()

    override fun login() {
        val ssoResp = Jsoup.connect(baseUrl + "login/cas").execute()
        val ssoDoc = ssoResp.parse()
        val ssoUrl = ssoDoc.selectFirst("#fm1").attr("action")
        ssoDoc.select("#username").attr("value", this.username)
        ssoDoc.select("#password").attr("value", this.password)
        val ssoData = ssoDoc.select("#fm1 input").map {
            HttpConnection.KeyVal.create(it.attr("name"), it.`val`())
        }

        val ssoResp2 =
            Jsoup.connect(ssoResp.url().toURI().resolve(ssoUrl).toString())
                .data(ssoData)
                .method(Connection.Method.POST)
                .execute()
        if(ssoResp2.url().toString() != this.baseUrl) {
            throw AuthenticationFailureException()
        }
        this.cookies = ssoResp2.cookies()
    }

    fun getPage(path: String): Document {
        val resp = Jsoup.connect(this.baseUrl + path).cookies(this.cookies).execute()
        if(resp.url().path == "/owl/login/") {
            throw NotAuthenticatedException()
        }
        return resp.parse()
    }

    override fun getCourses(): List<Course> {
        val doc = this.getPage("")
        return doc.select("a")
            .filter { it.attr("href").startsWith("/owl/c/") }
            .map { Course(it.text(), Regex("/c/(.*)/").find(it.attr("href"))!!.groupValues[1]) }
    }

    fun parseDateTime(str: String): LocalDateTime =
        LocalDateTime.parse(
            str.split("(")[0].trim(),
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))

    override fun getAssignments(courseId: String): List<AssignmentGroup> {
        val doc = this.getPage("/c/$courseId/")
        return doc.select("h3").map { heading ->
            val name = heading.text()

            val assignmentTable = heading.nextElementSibling()
            val assignments = assignmentTable.select("tr.told,tr.tnew").map { assignmentElem ->
                val cells = assignmentElem.children()
                Assignment(
                    cells[0].text(),
                    Regex(".*/c/(.*)/").find(cells[0].selectFirst("a").attr("href"))!!
                        .groupValues[1],
                    listOfNotNull(
                        cells[1].text().ifBlank { null }?.let {
                            Deadline(parseDateTime(it), cells[3].text().toDouble())
                        }),
                    cells[2].text().toDoubleOrNull(),
                )
            }

            AssignmentGroup(name, assignments)
        }
    }

    override fun getAssignmentPosts(assignmentId: String): List<Post> {
        assert(assignmentId.contains("/"))
        val doc = this.getPage("/c/$assignmentId/")
        return doc.select(".post").map { elem ->
            val header = elem.selectFirst(".phdr").text()
            val body = elem.selectFirst(".pbody pre")?.text()
            val pts = elem.selectFirst(".points")?.text()
            Post(
                header.split(" — ")[0],
                parseDateTime(header.split(" — ")[1].replace("modified ", "")),
                if(body == null) pts.orEmpty()
                else body + pts?.let { "\n\n" + it }.orEmpty()
            )
        }
    }
}