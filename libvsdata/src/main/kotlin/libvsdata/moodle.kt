// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
package libvsdata

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import org.jsoup.Connection
import org.jsoup.Jsoup
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

class MoodleClient(baseUrl: String, val username: String, val password: String)
    : AssignmentServiceClient, AuthenticatedClient {
    val baseUrl = if(baseUrl.endsWith("/")) baseUrl else "$baseUrl/"
    var token: String? = null
    var userId: Long? = null

    override fun login() {
        val resp = Jsoup.connect(baseUrl + "login/token.php")
            .data(mapOf(
                // APIs are often restricted only to some services, so we mimic the official app
                "service" to "moodle_mobile_app",
                "username" to username,
                "password" to password,
            ))
            .ignoreContentType(true)
            // This endpoint also seems to accept GET requests, but is's a TRAP!
            // It gives a different token that acts like the user doesn't have any privileges.
            .method(Connection.Method.POST)
            .execute()

        assert(resp.contentType().split(";")[0] == "application/json")
        val json = Json.parseToJsonElement(resp.body())

        if(json.obj["errorcode"]?.pstr == "invalidlogin") {
            throw AuthenticationFailureException()
        }
        token = json.obj["token"]!!.pstr

        // We need the user ID to call some functions, even for the current user
        val infoJson = callWsFunction("core_webservice_get_site_info")
        userId = infoJson.obj["userid"]!!.plong
    }

    override fun getCourses(): List<Course> {
        val json = callWsFunction("core_enrol_get_users_courses", mapOf(
            "userid" to userId.toString()
        ))

        return json.arr.map { course ->
            Course(
                course.obj["fullname"]!!.pstr,
                course.obj["id"]!!.plong.toString())
        }
    }

    override fun getAssignments(courseId: String): List<AssignmentGroup> {
        // We need two calls, one to get the assignments in the categories, and another to get
        // the assignment deadline from a flat assignment list
        // We could just use the second one without caring for assignment groups, but those are
        // often very useful in Moodle, for example to know if it's even your class
        val courseContentsJson = callWsFunction("core_course_get_contents", mapOf(
            "courseid" to courseId,
        ))
        val assignmentsJson = callWsFunction("mod_assign_get_assignments", mapOf(
            "courseids[0]" to courseId,
        ))
        val assignmentsById =
            assignmentsJson.obj["courses"]!!.arr[0].obj["assignments"]!!.arr
                .map { Pair(it.obj["cmid"]!!.plong, it.obj) }
                .toMap()
        val quizzesJson = callWsFunction("mod_quiz_get_quizzes_by_courses", mapOf(
            "courseids[0]" to courseId,
        ))
        val quizzesById =
            quizzesJson.obj["quizzes"]!!.arr
                .map { Pair(it.obj["coursemodule"]!!.plong, it.obj) }
                .toMap()

        return courseContentsJson.arr.map { group ->
            AssignmentGroup(
                group.obj["name"]!!.pstr,
                group.obj["modules"]!!.arr
                    .mapNotNull { module ->
                        when(module.obj["modname"]!!.pstr) {
                            "assign" -> {
                                // When the user doesn't have access, the item is in the general
                                // list but not in the assignment/quiz list
                                val assignmentJson = assignmentsById[module.obj["id"]!!.plong]
                                    ?: return@mapNotNull null
                                val statusJson =
                                    callWsFunction("mod_assign_get_submission_status", mapOf(
                                        "assignid" to assignmentJson["id"]!!.pstr,
                                        "userid" to userId.toString(),
                                    )).obj
                                Assignment(
                                    module.obj["name"]!!.pstr,
                                    "$courseId/a_${module.obj["id"]!!.plong}",
                                    listOf(
                                        Deadline(
                                            timestampToLDT(assignmentJson["duedate"]!!.plong),
                                            assignmentJson["grade"]!!.pdouble)
                                    ),
                                    statusJson["feedback"]?.obj
                                        ?.get("grade")?.obj
                                        ?.get("grade")?.pstr
                                        ?.toDouble())
                            }
                            "quiz" -> {
                                val quizJson = quizzesById[module.obj["id"]!!.plong]
                                    ?: return@mapNotNull null
                                val quizAttemptsJson =
                                    callWsFunction("mod_quiz_get_user_attempts", mapOf(
                                        "quizid" to quizJson["id"]!!.pstr,
                                        "userid" to userId.toString(),
                                    ))
                                        .obj["attempts"]!!.arr
                                        .maxByOrNull { it.obj["attempt"]!!.plong }
                                val pts = quizAttemptsJson?.obj?.get("sumgrades")
                                Assignment(
                                    module.obj["name"]!!.pstr,
                                    "$courseId/q_${module.obj["id"]!!.plong}",
                                    listOf(
                                        Deadline(
                                            timestampToLDT(quizJson["timeclose"]!!.plong),
                                            quizJson["grade"]!!.pdouble)
                                    ),
                                    if(pts?.pstr == "null") null else pts?.pdouble)
                            }
                            else -> null
                        }
                    }
            )
        }.filter { it.assignments.isNotEmpty() }
    }

    override fun getAssignmentPosts(assignmentId: String): List<Post> {
        val (courseId, assignmentSubId) = assignmentId.split("/")
        val (modType, modId) = assignmentSubId.split("_")
        return when(modType) {
            "a" -> {
                val assignmentsJson = callWsFunction("mod_assign_get_assignments", mapOf(
                    "courseids[0]" to courseId,
                ))
                val assignmentJson =
                    assignmentsJson.obj["courses"]!!.arr[0].obj["assignments"]!!.arr
                        .find { it.obj["cmid"]!!.plong == modId.toLong() }!!.obj
                listOf(
                    Post(null,
                         timestampToLDT(assignmentJson["timemodified"]!!.plong),
                         assignmentJson["intro"]!!.pstr)
                )
            }
            "q" -> {
                val quizzesJson = callWsFunction("mod_quiz_get_quizzes_by_courses", mapOf(
                    "courseids[0]" to courseId,
                ))
                val quizJson =
                    quizzesJson.obj["quizzes"]!!.arr
                        .find { it.obj["coursemodule"]!!.plong == modId.toLong() }!!.obj
                listOf(Post(null, null, quizJson["intro"]!!.pstr))
            }
            else -> throw NotFoundException()
        }
    }

    // Just hope it's in the default timezone
    // TODO: Can Moodle tell us its timezone?
    fun timestampToLDT(timestamp: Long): LocalDateTime =
        Instant.ofEpochSecond(timestamp)
            .atZone(ZoneId.systemDefault())
            .toLocalDateTime()

    fun callWsFunction(funcName: String, params: Map<String, String> = mapOf()): JsonElement {
        if(token == null) {
            throw NotAuthenticatedException()
        }
        val json = getJson("webservice/rest/server.php", mapOf(
            "wstoken" to token!!,
            "moodlewsrestformat" to "json",
            "wsfunction" to funcName,
        ) + params)
        if(json is JsonObject) {
            val errCode = json.obj["errorcode"]?.pstr
            if(errCode != null) {
                if(errCode == "invalidtoken") {
                    throw NotAuthenticatedException()
                } else {
                    throw Exception("Moodle WS error: $errCode")
                }
            }
        }
        return json
    }

    fun getJson(path: String, params: Map<String, String> = mapOf()): JsonElement {
        // Jsoup's certainly not primarily meant for this, but Java11's HttpClient isn't
        // on Android, so Jsoup's actually the best "cross-platform" HTTP client we have.
        val resp = Jsoup.connect(baseUrl + path)
            .data(params)
            .ignoreContentType(true)
            .execute()
        assert(resp.contentType().split(";")[0] == "application/json")
        return Json.parseToJsonElement(resp.body())
    }
}