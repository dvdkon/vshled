// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
@file:UseSerializers(LocalDateTimeAsStringSerializer::class)
package libvsdata

import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.time.LocalDateTime

class NotAuthenticatedException : Exception()
class AuthenticationFailureException : Exception()
class NotPermittedException : Exception()
class NotFoundException : Exception()

@Serializable data class Course(val name: String, val id: String)
@Serializable data class AssignmentGroup(val name: String?, val assignments: List<Assignment>)
@Serializable data class Deadline(val dateTime: LocalDateTime, val pointsMax: Double)
@Serializable data class Assignment(
    val name: String,
    val id: String,
    val deadlines: List<Deadline>,
    val pointsGained: Double?) // TODO: "Decimal" type for greater accuracy
@Serializable data class Post(
    val author: String?,
    val postedOn: LocalDateTime?,
    val body: String,
)

interface AuthenticatedClient {
    fun login()
}

interface AssignmentServiceClient {
    fun getCourses(): List<Course>
    fun getAssignments(courseId: String): List<AssignmentGroup>
    fun getAssignmentPosts(assignmentId: String): List<Post>
}