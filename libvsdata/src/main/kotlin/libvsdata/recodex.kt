// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
@file:UseSerializers(LocalDateTimeAsStringSerializer::class)
package libvsdata

import kotlinx.serialization.UseSerializers
import kotlinx.serialization.json.*
import libvsdata.sis.splitGetParams
import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.helper.HttpConnection
import java.net.URI
import java.time.LocalDateTime
import java.time.ZoneOffset

class RecodexClient(val auth: Authentication) : AssignmentServiceClient, AuthenticatedClient {
    val baseUrl = "https://recodex.mff.cuni.cz/api/v1/"
    var token: String? = null
    var userId: String? = null

    sealed class Authentication {
        data class Cas(val username: String, val password: String) : Authentication()
    }

    override fun login() {
        when(this.auth) {
            is Authentication.Cas -> {
                val ssoUrl = "https://idp.cuni.cz/cas/login?service=https%3A%2F%2Frecodex.mff.cuni.cz%2Fcas-auth-ext%2F&renew=true"
                val ssoResp = Jsoup.connect(ssoUrl).execute()
                val ssoDoc = ssoResp.parse()
                ssoDoc.select("#username").attr("value", this.auth.username)
                ssoDoc.select("#password").attr("value", this.auth.password)
                val ssoUrl2 = ssoDoc.select("#fm1").attr("action")
                val ssoData = ssoDoc.select("#fm1 input").map {
                    HttpConnection.KeyVal.create(it.attr("name"), it.`val`())
                }

                val ssoResp2 =
                    Jsoup.connect(ssoResp.url().toURI().resolve(ssoUrl2).toString())
                        .cookies(ssoResp.cookies())
                        .data(ssoData)
                        .method(Connection.Method.POST)
                        .execute()
                if(ssoResp2.url().host != URI(baseUrl).host) {
                    throw AuthenticationFailureException()
                }
                val token = splitGetParams(ssoResp2.url().query)["token"]!!

                val postAuthJson = getJson("login/cas-uk", data = buildJsonObject {
                    put("token", token)
                })
                val payload = postAuthJson.jsonObject["payload"]!!.jsonObject
                this.token = payload["accessToken"]!!.jsonPrimitive.content
                this.userId = payload["user"]!!.jsonObject["id"]!!.jsonPrimitive.content
            }
        }
    }

    fun getJson(path: String, params: Map<String, String> = mapOf(), data: JsonElement? = null): JsonElement {
        var req = Jsoup.connect(this.baseUrl + path)
            .data(params)
            .header("Authorization", "Bearer " + this.token.orEmpty())
            .ignoreContentType(true)
            .ignoreHttpErrors(true)
        if(data != null) {
            req = req
                .header("Content-Type", "application/json")
                .requestBody(data.toString())
                .method(Connection.Method.POST)
        }
        val resp = req.execute()
        if(resp.statusCode() == 403) {
            throw NotAuthenticatedException()
        } else if(resp.statusCode() != 200) {
            throw Exception("Unexpected HTTP return code! " + resp.statusMessage() + "; Body:\n" + resp.body())
        }
        return Json.parseToJsonElement(resp.body())
    }

    // TODO: Give both?
    fun getCzechOrOtherText(json: JsonElement): JsonObject {
        return json.jsonArray.minByOrNull { if(it.jsonObject["locale"]!!.jsonPrimitive.content == "cs") 0 else 0 }!!.jsonObject
    }

    override fun getCourses(): List<Course> {
        val json = getJson("groups")
        return json.jsonObject["payload"]!!.jsonArray.map {
            Course(
                getCzechOrOtherText(it.jsonObject["localizedTexts"]!!)
                        ["name"]!!.jsonPrimitive.content,
                it.jsonObject["id"]!!.jsonPrimitive.content,
            )
        }
    }

    override fun getAssignments(courseId: String): List<AssignmentGroup> {
        val assignmentsJson = getJson("groups/$courseId/assignments")
        val statsJson = getJson("groups/$courseId/students/stats")
        val selfStats =
            statsJson.jsonObject["payload"]!!.jsonArray.map { it.jsonObject }
                .find { it["userId"]!!.jsonPrimitive.content == this.userId }!!
        val selfAssignmentStats = selfStats.jsonObject["assignments"]!!.jsonArray.map { it.jsonObject }
        val assignments = assignmentsJson.jsonObject["payload"]!!.jsonArray.map { assignment ->
            val obj = assignment.jsonObject
            val id = obj["id"]!!.jsonPrimitive.content
            val texts = getCzechOrOtherText(obj["localizedTexts"]!!)
            Assignment(
                texts["name"]!!.jsonPrimitive.content,
                id,
                listOfNotNull(
                    Deadline(
                        LocalDateTime.ofEpochSecond(
                            obj["firstDeadline"]!!.jsonPrimitive.long, 0, ZoneOffset.UTC), // TODO?
                        obj["maxPointsBeforeFirstDeadline"]!!.jsonPrimitive.double),
                    if(!obj["allowSecondDeadline"]!!.jsonPrimitive.boolean) null else
                        Deadline(
                            LocalDateTime.ofEpochSecond(
                                obj["secondDeadline"]!!.jsonPrimitive.long, 0, ZoneOffset.UTC),
                            obj["maxPointsBeforeSecondDeadline"]!!.jsonPrimitive.double),
                ),
                selfAssignmentStats
                    .find { it["id"]!!.jsonPrimitive.content == id }!!
                    ["points"]!!.jsonObject["gained"]!!.jsonPrimitive.doubleOrNull ?: 0.0
            )
        }
        return listOf(AssignmentGroup(null, assignments))
    }

    override fun getAssignmentPosts(assignmentId: String): List<Post> {
        val json = getJson("exercise-assignments/$assignmentId")
        val text = (
            getCzechOrOtherText(json.jsonObject["payload"]!!.jsonObject["localizedTexts"]!!)
                ["text"]!!.jsonPrimitive.content)
        return listOf(Post(null, null, text))
    }
}