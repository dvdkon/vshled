// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
package libvsdata

import kotlinx.serialization.json.*

// Shorter accessors for JSON, so that the important parts of code aren't drowned out
// by long conversions

val JsonElement.obj get() = this.jsonObject
val JsonElement.arr get() = this.jsonArray
val JsonElement.pstr get() = this.jsonPrimitive.content
val JsonElement.pint get() = this.jsonPrimitive.int
val JsonElement.plong get() = this.jsonPrimitive.long
val JsonElement.pfloat get() = this.jsonPrimitive.float
val JsonElement.pdouble get() = this.jsonPrimitive.double