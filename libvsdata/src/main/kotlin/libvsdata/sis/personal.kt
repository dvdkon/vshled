// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
@file:UseSerializers(LocalDateAsStringSerializer::class)
package libvsdata.sis

import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import libvsdata.LocalDateAsStringSerializer
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Serializable
data class UserInfo(
    val surname: String?,
    val firstName: String?,
    val middleName: String?,
    val maidenName: String?,
    val degreePrefix: String?,
    val degreeSuffix: String?,
    val addresses: List<String>,
    val contactAddress: String?,
    val dormitory: String?,
    val dormitoryRoom: String?,
    val phoneNums: List<String>,
    val mobilePhoneNum: String?,
    val personalEmail: String?,
    val website: String?,
    val bankAccount: String?,
    val bankAccountSpecificSymbol: String?,
    val personId: Int?,
    val badgeId: String?,
    val badgeStatus: String?,
    val login: String?,
    val faculty: String?,
    val studyProgramme: String?,
    val studyPlan: String?,
    val studyDiscipline: String?,
    val year: Int?,
    val studyGroup: Int?,
    val studyDegree: String?,
    val studyForm: String?,
    val studyStatus: String?,
    val studyStatusValidity: String?,
    val enrolmentDate: LocalDate?,
    val authentication: String?,
)

fun getSelfUserInfo(client: SisClient): UserInfo {
    val doc = client.getPage("omne/index.php", mapOf("do" to "info"))
    val table = doc.selectFirst(".pageBlock .tab2 .tab2")
    val rows = parseKeyValueTable(table)
    return UserInfo(
        rows["Příjmení:"],
        rows["Jméno:"],
        rows["Prostřední jméno:"],
        rows["Rodné příjmení:"],
        rows["Titul:"],
        rows["Titul za:"],
        listOfNotNull(rows["Trvalá adresa:"], rows["Další adresa:"]),
        rows["Doručovací adresa:"],
        rows["Kolej:"],
        rows["Pokoj:"],
        listOfNotNull(rows["Telefon:"], rows["Telefon 2:"]),
        rows["Mobilní telefon:"],
        rows["Osobní e-mail:"],
        rows["URL:"],
        rows["Číslo účtu:"],
        rows["Specifický symbol:"],
        rows["Číslo osoby (UKČO):"]?.toInt(),
        rows["Číslo průkazu (čárový kód):"],
        rows["Stav průkazu:"],
        rows["Login:"],
        rows["Fakulta:"],
        rows["Studijní program:"],
        rows["Studijní plán:"],
        rows["Studijní obor:"],
        rows["Ročník:"]?.toInt(),
        rows["Studijní skupina:"]?.toInt(),
        rows["Druh studia:"],
        rows["Forma studia:"],
        rows["Studijní stav:"],
        rows["Platnost stud. stavu:"],
        LocalDate.parse(rows["Datum zápisu:"], DateTimeFormatter.ofPattern("dd.MM.yyyy")),
        rows["Autentizace:"],
    )
}

@Serializable
data class StudyEnrollment(
    val school: String,
    val faculty: String,
    val enrollmentDate: LocalDate,
    val programmeId: String,
    val studyDegree: String,
    val standardStudyLength: Int,
    val terminationDate: LocalDate?,
    val terminationType: String?,
    val fromDate: LocalDate,
    val toDate: LocalDate?,
    val disciplines: List<String>,
    val status: String,
    val form: String,
    val unpaidStudyPeriod: String?,
)

fun getSelfStudyEnrollments(client: SisClient): List<StudyEnrollment> {
    fun parseDate(d: String): LocalDate {
        return LocalDate.parse(d, DateTimeFormatter.ofPattern("dd.MM.yyyy"))
    }

    val doc = client.getPage("omne/index.php", mapOf("do" to "sims"))
    return parseDataTable(doc.selectFirst("#sims")).map { cols ->
        StudyEnrollment(
            cols["Škola"]!!,
            cols["Fakulta"]!!,
            parseDate(cols["Zápis"]!!),
            cols["Program"]!!,
            cols["Druh"]!!,
            cols["Std. délka studia"]!!.toInt(),
            cols["Ukončení"]?.let { parseDate(it) },
            cols["Způsob ukončení"],
            parseDate(cols["Etapa od"]!!),
            cols["Etapa do"]?.let { parseDate(it) },
            listOfNotNull(cols["Obor 1"], cols["Obor 2"]),
            cols["Stav"]!!,
            cols["Forma"]!!,
            cols["Nezpoplatněná doba studia"],
        )
    }
}
