// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
@file:UseSerializers(
    LocalDateAsStringSerializer::class,
    LocalTimeAsStringSerializer::class,
    DurationAsSecondsSerializer::class)

package libvsdata.sis

import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import libvsdata.*
import org.jsoup.Connection
import org.jsoup.nodes.Element
import org.jsoup.nodes.TextNode
import java.nio.charset.Charset
import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@Serializable
enum class ScheduledWeeks { AllWeeks, EvenWeeks, OddWeeks, Irregular }

@Serializable
data class ScheduledClass(
    val id: String,
    val subId: String,
    val subjectId: String,
    val subjectName: String,
    val weekday: Int?,
    val startTime: LocalTime?,
    val room: String?,
    val duration: Duration?,
    val firstWeek: Int?,
    val firstWeekOneShot: Int?,
    val weekCount: Int?,
    val scheduledWeeks: ScheduledWeeks,
    val teachers: String,
)

fun parseScheduleCsv(csv: ByteArray): List<ScheduledClass> {
    val respStr = String(csv, Charset.forName("Windows-1250"))
    return respStr.split("\n").mapIndexedNotNull { i, rowStr ->
        if(i == 0 || rowStr.isBlank()) return@mapIndexedNotNull null // Skip header
        val row = rowStr.split(";").map { it.ifBlank { null } }
        ScheduledClass(
            id = row[0]!!,
            subId = row[1]!!,
            subjectId = row[2]!!,
            subjectName = row[3]!!,
            weekday = row[4]?.toInt(),
            startTime = row[5]?.let { LocalTime.ofSecondOfDay(it.toLong() * 60) },
            room = row[6],
            duration = row[7]?.let { Duration.ofMinutes(it.toLong()) },
            firstWeek = row[8]?.toInt(),
            firstWeekOneShot = row[9]?.toInt(),
            weekCount = row[10]?.toInt(),
            scheduledWeeks = when(row[11]) {
                null -> ScheduledWeeks.AllWeeks
                "sude" -> ScheduledWeeks.EvenWeeks
                "liche" -> ScheduledWeeks.OddWeeks
                else -> throw UnhandledInputException("Scheduled weeks", row[11].orEmpty())
            },
            teachers = row[12]!!,
        )
    }
}

fun checkCsvResp(client: SisClient, resp: Connection.Response) {
    if(resp.contentType() != "application/csv") {
        if(resp.contentType().split(";")[0] == "text/html") {
            if(!isAuthenticated(resp.parse())) {
                if(client.auth is SisClient.Authentication.Anonymous)
                    throw NotPermittedException()
                else throw NotAuthenticatedException()
            }
            throw NotFoundException()
        }
        throw Exception(
            "Unexpected content type! Expected application/csv, got ${resp.contentType()}")
    }
}

fun getSelfSchedule(
    client: SisClient,
    year: AcademicYear,
    semester: Semester,
): List<ScheduledClass> {
    val resp = client.getPageRaw("rozvrhng/roz_muj_macro.php", mapOf(
        "rezim" to "vse",
        "csv" to "1",
        "skr" to year.startYear.toString(),
        "sem" to when(semester) {
            Semester.Winter -> "1"; Semester.Summer -> "2"
        },
        // Faculty ID influences the UI (dimming non-matching classes), but does nothing
        // for the exports
    ), nonHtml = true)
    checkCsvResp(client, resp)
    return parseScheduleCsv(resp.bodyAsBytes())
}

fun getSubjectSchedule(
    client: SisClient,
    year: AcademicYear,
    semester: Semester,
    facultyId: String,
    subjectId: String,
): List<ScheduledClass> {
    val resp = client.getPageRaw("rozvrhng/roz_predmet_macro.php", mapOf(
        "csv" to "1",
        "skr" to year.startYear.toString(),
        "sem" to when(semester) {
            Semester.Winter -> "1"; Semester.Summer -> "2"
        },
        "fak" to facultyId,
        "predmet" to subjectId,
    ), nonHtml = true)
    checkCsvResp(client, resp)
    return parseScheduleCsv(resp.bodyAsBytes())
}

@Serializable
data class ScheduledClassDay(
    val id: String,
    val subId: String,
    val subjectId: String,
    val subjectName: String,
    val weekday: Int,
    val day: LocalDate,
    val startTime: LocalTime,
    val room: String?,
    val duration: Duration,
    val teachers: String,
)

fun parseDailyScheduleCsv(csv: ByteArray): List<ScheduledClassDay> {
    val respStr = String(csv, Charset.forName("Windows-1250"))
    return respStr.split("\n").mapIndexedNotNull { i, rowStr ->
        if(i == 0 || rowStr.isBlank()) return@mapIndexedNotNull null // Skip header
        val row = rowStr.split(";").map { it.ifBlank { null } }
        ScheduledClassDay(
            id = row[0]!!,
            subId = row[1]!!,
            subjectId = row[2]!!,
            subjectName = row[3]!!,
            weekday = row[4]!!.toInt(),
            day = LocalDate.parse(row[5]!!, DateTimeFormatter.ofPattern("d.M.yyyy")),
            startTime = row[6]!!.let { LocalTime.ofSecondOfDay(it.toLong() * 60) },
            room = row[7],
            duration = row[8]!!.let { Duration.ofMinutes(it.toLong()) },
            teachers = row[9]!!,
        )
    }
}

fun getSelfDailySchedule(
    client: SisClient,
    year: AcademicYear,
    semester: Semester,
): List<ScheduledClassDay> {
    val resp = client.getPageRaw("rozvrhng/roz_muj_micro.php", mapOf(
        "rezim" to "vse",
        "csv" to "1",
        "skr" to year.startYear.toString(),
        "sem" to when(semester) {
            Semester.Winter -> "1"; Semester.Summer -> "2"
        },
    ), nonHtml = true)
    checkCsvResp(client, resp)
    return parseDailyScheduleCsv(resp.bodyAsBytes())
}

fun getSubjectDailySchedule(
    client: SisClient,
    year: AcademicYear,
    semester: Semester,
    facultyId: String,
    subjectId: String,
): List<ScheduledClassDay> {
    val resp = client.getPageRaw("rozvrhng/roz_predmet_micro.php", mapOf(
        "csv" to "1",
        "skr" to year.startYear.toString(),
        "sem" to when(semester) {
            Semester.Winter -> "1"; Semester.Summer -> "2"
        },
        "fak" to facultyId,
        "predmet" to subjectId,
    ), nonHtml = true)
    checkCsvResp(client, resp)
    return parseDailyScheduleCsv(resp.bodyAsBytes())
}

@Serializable
enum class ClassType {
    Lecture,
    Seminar,
    BlockLecture,
    BlockSeminar,
    IrregularLecture,
    IrregularSeminar,
    Preparation,
    NonVerifiedLecture,
    NonVerifiedSeminar,
    Reservation,
    RepeatReservation,
    SemestralReservation,
    ToScheduleNotWanted,
    ToScheduleNotCompatible,
    ToSchedulePossible,
    ToScheduleNotEnrolled,
    ToScheduleNotPossible,
    Consultation,
    NewlyAdded,
}

// This is an awful hack that doesn't deal with class subIDs being given different types.
// That only happens in weird cases, not to say it doesn't happen
fun parseClassTypesByColour(boxes: List<Element>) =
    boxes.map { box ->
        val link = box.selectFirst("a").attr("href")
        val id = splitUrlGetParams(link)["gl"]!!
        val colour = Regex("background-color: #([A-Za-z0-9]+);.*")
            .matchEntire(box.attr("style"))!!
            .groupValues[1]
        val type = when(colour.lowercase()) {
            // From rozvrhng/roz_barvy.php
            "5095cb" -> ClassType.Lecture
            "92c4df" -> ClassType.Seminar
            // XXX: Can't distinguish bi-weekly lectures and seminars
            "bce2eb" -> ClassType.Seminar
            "66aa77" -> ClassType.BlockLecture
            "aaddbb" -> ClassType.BlockSeminar
            "cd69c9" -> ClassType.IrregularLecture
            "ff83fa" -> ClassType.IrregularSeminar
            "708888" -> ClassType.Preparation
            "cc4444" -> ClassType.NonVerifiedLecture
            "ff6666" -> ClassType.NonVerifiedSeminar
            "ffbb55" -> ClassType.Reservation
            "ff8844" -> ClassType.RepeatReservation
            "ffff88" -> ClassType.SemestralReservation
            "ffaa55" -> ClassType.ToScheduleNotWanted
            "ff5555" -> ClassType.ToScheduleNotCompatible
            "ffff00" -> ClassType.ToSchedulePossible
            "ffffaa" -> ClassType.ToScheduleNotEnrolled
            "ffffff" -> ClassType.ToScheduleNotPossible
            "9970bb" -> ClassType.Consultation
            "99ff99" -> ClassType.NewlyAdded
            else -> throw UnhandledInputException("Scheduled class colour", colour)
        }
        Pair(id, type)
    }.toMap()

fun getSelfScheduleClassTypes(
    client: SisClient,
    year: AcademicYear,
    semester: Semester,
): Map<String, ClassType> {
    val doc = client.getPage("rozvrhng/roz_muj_macro.php", mapOf(
        "rezim" to "vse",
        "skr" to year.startYear.toString(),
        "sem" to when(semester) {
            Semester.Winter -> "1"; Semester.Summer -> "2"
        },
    ))

    return parseClassTypesByColour(doc.select(".ramecek"))
}

fun getSubjectScheduleClassTypes(
    client: SisClient,
    year: AcademicYear,
    semester: Semester,
    facultyId: String,
    subjectId: String,
): Map<String, ClassType> {
    val doc = client.getPage("rozvrhng/roz_predmet_macro.php", mapOf(
        "skr" to year.startYear.toString(),
        "sem" to when(semester) {
            Semester.Winter -> "1"; Semester.Summer -> "2"
        },
        "fak" to facultyId,
        "predmet" to subjectId,
    ))
    return parseClassTypesByColour(doc.select(".ramecek"))
}

@Serializable
data class StudentGroup(
    val kindId: String?,
    val programmeId: String?,
    val year: Int?,
    val disciplineId: String?,
    val specialisationId: String?,
    val parallelGroupId: String?,
    val studyGroupId: String?,
    val teachingMethodId: String?,
)

@Serializable
data class ScheduledClassDetail(
    val id: String,
    val subId: String,
    val subject: NamedWithId,
    val type: ClassType,
    val scheduledWeeks: ScheduledWeeks,
    val weekday: DayOfWeek,
    val startTime: LocalTime,
    val room: String?,
    val duration: Duration,
    val teachers: List<NamedWithId>,
    val studentGroups: List<StudentGroup>,
    val language: String,
    val publicNote: String?,
    val privateNote: String?,
    val studentCount: Int,
    val maxStudentCount: Int?,
    val electronicEnrollment: Boolean?,
    val userMetadata: String?,
    val schedulingState: String?,
    val departmentName: String,
    val followUpClassId: String?,
)

fun getScheduledClassDetails(
    client: SisClient,
    year: AcademicYear,
    semester: Semester,
    facultyId: String,
    scheduledClassId: String,
): List<ScheduledClassDetail> {
    val doc = client.getPage("rozvrhng/roz_predmet_gl.php", mapOf(
        "skr" to year.startYear.toString(),
        "sem" to when(semester) {
            Semester.Winter -> "1"; Semester.Summer -> "2"
        },
        "fak" to facultyId,
        "gl" to scheduledClassId,
    ))
    val errorText = doc.selectFirst(".error_text")
    if(errorText != null && errorText.text().startsWith("Vybraný lístek pochází ze semestru ,")) {
        throw NotFoundException()
    }

    val subjectTitle = doc.selectFirst(".form_div_title .link1").text()
    val subjectTitleMatch = Regex("(.*) ([^ ]*)$").matchEntire(subjectTitle)!!
    val subjectName = subjectTitleMatch.groupValues[1]
    val subjectId = subjectTitleMatch.groupValues[2]

    return doc.select(".pageBlock").mapNotNull { subBlock ->
        val title = subBlock.selectFirst(".form_div_title").text()
        if(!title.startsWith("Podrobný přehled ")) {
            return@mapNotNull null
        }

        val tbl = parseKeyValueTableDom(subBlock.selectFirst(".tab1"))

        val timeSplit = tbl["Rozvržení:"]!!.text().split(" ")

        ScheduledClassDetail(
            id = scheduledClassId,
            subId = title.replace("Podrobný přehled ", ""),
            subject = NamedWithId(subjectName, subjectId),
            type = when(val s = tbl["Typ:"]!!.text()) {
                "Přednáška" -> ClassType.Lecture
                "Cvičení" -> ClassType.Seminar
                else -> throw UnhandledInputException("Class type", s)
            },
            scheduledWeeks = when(val s = tbl["Opakování:"]!!.text().trim()) {
                "Každý týden" -> ScheduledWeeks.AllWeeks
                "Liché týdny" -> ScheduledWeeks.OddWeeks
                "Sudé týdny" -> ScheduledWeeks.EvenWeeks
                "Nepravidelná" -> ScheduledWeeks.Irregular
                else -> throw UnhandledInputException("Scheduled weeks", s)
            },
            weekday = when(timeSplit[0]) {
                "Pondělí" -> DayOfWeek.MONDAY
                "Úterý" -> DayOfWeek.TUESDAY
                "Středa" -> DayOfWeek.WEDNESDAY
                "Čtvrtek" -> DayOfWeek.THURSDAY
                "Pátek" -> DayOfWeek.FRIDAY
                "Sobota" -> DayOfWeek.SATURDAY
                "Neděle" -> DayOfWeek.SUNDAY
                else -> throw UnhandledInputException("Weekday", timeSplit[0])
            },
            startTime = LocalTime.parse(timeSplit[1], DateTimeFormatter.ofPattern("H:mm")),
            room = tbl["Místo výuky:"]!!.text().ifBlank { null },
            duration = Duration.ofMinutes(tbl["Délka:"]!!.text().toLong()),
            // Chunks of: Name, teacher link, whitespace, subjects link
            // Very stable, won't break at all
            teachers = tbl["Učitelé:"]!!.childNodes().chunked(4).map {
                NamedWithId(
                    (it[0] as TextNode)
                        .text()
                        .replace(Regex("^, "), "").trim(),
                    getKodFromLink(it[1] as Element)
                )
            },
            studentGroups = tbl["Studenti:"]!!.select("a").map {
                val params = splitUrlGetParams(it.attr("href"))
                StudentGroup(
                    kindId = params["druh"],
                    programmeId = params["program"],
                    year = params["rocnik"]?.toInt(),
                    disciplineId = params["kombinace"],
                    specialisationId = params["specializace"],
                    parallelGroupId = params["paralelka"],
                    studyGroupId = params["kruh"],
                    teachingMethodId = params["forma"],
                )
            },
            language = tbl["Jazyk výuky:"]!!.text(),
            publicNote = tbl["Veřejná poznámka k rozvrhu:"]!!.text().ifBlank { null },
            privateNote = tbl["Vzkaz přihlášeným studentům:"]?.text()?.ifBlank { null },
            studentCount = tbl["Aktuální počet studentů:"]!!.text().toInt(),
            maxStudentCount = tbl["Maximální počet studentů:"]!!.text().toIntOrNull(),
            electronicEnrollment =
            when(val s = tbl["Elektronický zápis studentů:"]?.text()) {
                "ano" -> true
                "ne" -> false
                null -> null
                else -> throw UnhandledInputException("Elektronický zápis studentů", s)
            },
            userMetadata = tbl["Uživatelský příznak:"]?.text()?.ifBlank { null },
            schedulingState = tbl["Stav rozvržení:"]?.text()?.ifBlank { null },
            departmentName = tbl["Katedra:"]!!.text(),
            followUpClassId = tbl["Návazný lístek:"]!!.text(),
        )
    }
}
