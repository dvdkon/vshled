// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
@file:UseSerializers(LocalDateTimeAsStringSerializer::class)

package libvsdata.sis

import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import libvsdata.LocalDateTimeAsStringSerializer
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Serializable
data class ScheduleEvent(
    val from: LocalDateTime,
    val to: LocalDateTime,
    val facultyName: String,
    val typeName: String,
    val note: String?,
)

fun getYearSchedule(client: SisClient, eventType: String?): List<ScheduleEvent> {
    val doc = client.getPage("harmonogram/index.php", mapOf(
        "do" to "filtr",
        "moje" to "0",
        "fak" to "",
        "skr" to "",
        "hartyp" to eventType.orEmpty(),
    ))
    val dateFmt = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
    return parseDataTable(doc.select(".tab1").last()).map { row ->
        ScheduleEvent(
            from = LocalDateTime.parse(row["Od"], dateFmt),
            to = LocalDateTime.parse(row["Do"], dateFmt),
            facultyName = row["Fakulta"]!!,
            typeName = row["Název"]!!,
            note = row["Poznámka"],
        )
    }
}

@Serializable
data class SemesterStartDate(
    val academicYear: AcademicYear,
    val semester: Semester,
    val date: LocalDateTime,
)

// This is a very ugly hack to get the starts of semesters
// Sometimes different faculties start classes on different days, so we just take the first date,
// I don't want API clients to have to deal with choosing which faculty's semester they're going by,
// at least not by default
fun getSemesterStartDates(client: SisClient) =
    getYearSchedule(client, "VYUKA")
        .mapNotNull {
            val lowerNote = it.note?.lowercase() ?: ""
            val winterSemester = lowerNote.contains("zimní") || lowerNote.contains("zs")
            val summerSemester = lowerNote.contains("letní") || lowerNote.contains("ls")
            when {
                winterSemester -> Pair(Pair(AcademicYear(it.from.year), Semester.Winter), it.from)
                summerSemester -> Pair(Pair(AcademicYear(it.from.year - 1), Semester.Summer), it.from)
                else -> null
            }
        }
        .groupBy { it.first }
        .map { SemesterStartDate(it.key.first, it.key.second, it.value.minOf { d -> d.second }) }