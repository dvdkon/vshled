// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2022 David Koňařík
package libvsdata.sis

import kotlinx.serialization.Serializable
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.util.*

@Serializable
enum class CompletionRequirements {
    // TODO: Fill out the rest: https://cuni.cz/UK-4457.html
    FinalExam,
    Credit,
    ClassifiedCredit,
    MaybeFinalExamAndCredit, // TODO: What is Z(+Zk) really?
    FinalExamAndCredit,
    DoctoralExam,
    Colloquium,
    ColloquiumAndFinalExam,
    Thesis,
    Dissertation,
    RigorousThesis,
    DoctoralStudyEvaluation,
    RigorousExam,
    KlauzurniPrace,
    Other,
    Unspecified,
}

@Serializable
data class SemesterSpecificSubjectInfo(
    val lectureBlocks: Int,
    val seminarBlocks: Int,
    val requirements: CompletionRequirements?,
)

@Serializable
data class SubjectSimple(
    val id: String,
    val name: String,
    val semesters: Set<Semester>,
    val semesterSpecific: Map<Semester, SemesterSpecificSubjectInfo>,
    val department: String,
    val faculty: String,
)

fun parseCompletionRequirements(str: String): CompletionRequirements {
    return when(str) {
        "Zk" -> CompletionRequirements.FinalExam
        "Z" -> CompletionRequirements.Credit
        "KZ" -> CompletionRequirements.ClassifiedCredit
        "Z(+Zk)" -> CompletionRequirements.MaybeFinalExamAndCredit
        "Z+Zk" -> CompletionRequirements.FinalExamAndCredit
        "SZ" -> CompletionRequirements.DoctoralExam // XXX: Isn't on the official list
        "Kv" -> CompletionRequirements.Colloquium
        "Kv+Zk" -> CompletionRequirements.ColloquiumAndFinalExam
        "Dipl" -> CompletionRequirements.Thesis
        "Dise" -> CompletionRequirements.Dissertation
        "Rigo" -> CompletionRequirements.RigorousThesis
        "Hodn" -> CompletionRequirements.DoctoralStudyEvaluation
        "RZ" -> CompletionRequirements.RigorousExam
        "KLP" -> CompletionRequirements.KlauzurniPrace
        "Jiné" -> CompletionRequirements.Other
        "---" -> CompletionRequirements.Unspecified
        else -> throw UnhandledInputException("Subject requirements", str)
    }
}

fun parseSemesters(str: String): Set<Semester> {
    return when(str) {
        "zimní" -> setOf(Semester.Winter)
        "letní" -> setOf(Semester.Summer)
        "oba" -> setOf(Semester.Summer, Semester.Winter)
        else -> throw UnhandledInputException("Semestr", str)
    }
}

fun parseSemesterSpecificSubjectInfo(lines: List<String>): Map<Semester, SemesterSpecificSubjectInfo> {
    return lines.flatMap { line_ ->
        val line = line_.replace(Regex("\\[HT\\]$"), "") // Remove hint
        val (semesters, rest) =
            if(line.contains(":")) {
                val split = line.split(":")
                val semester =
                    when (val semesterStr = split[0].trim()) {
                        "zimní s." -> Semester.Winter
                        "letní s." -> Semester.Summer
                        else -> throw UnhandledInputException("Semester", semesterStr)
                    }
                Pair(listOf(semester), split[1].trim())
            } else {
                Pair(listOf(Semester.Winter, Semester.Summer), line)
            }
        val restSplit = rest.split(",")
        val classesSplit = restSplit[0].split("/")
        val requirementsStr = restSplit[1].trim()
        val info = SemesterSpecificSubjectInfo(
            classesSplit[0].toInt(),
            classesSplit[1].toInt(),
            parseCompletionRequirements(requirementsStr)
        )
        semesters.map { Pair(it, info) }
    }.toMap()
}

fun searchSubjects(
    client: SisClient, facultyId: String?,
    name: String?, id: String?, exactMatch: Boolean = false,
): List<SubjectSimple> {
    fun inner(page: Int): Pair<Boolean, List<SubjectSimple>> {
        val doc = client.getPage("predmety/index.php", mapOf(
            "do" to "search",
            "b" to "Hledej",
            "table-force-export" to "1",
            // Can be anything except for the recognised formats
            // It's more likely to be a bug than anything, but it's a useful bug
            "table-force-export-type" to "html",
            "pocet" to "1000",
            "stev_page" to page.toString(),
            "fac" to facultyId.orEmpty(),
            "nazev" to name.orEmpty(),
            "kod" to id.orEmpty(),
            "match" to if(exactMatch) "exact" else "substring",
        ))
        val hasNextPage = doc.select("#page_div .link1").size > 0
        val data = parseDataTable(doc.selectFirst(".tab1")).map { cols ->
            // Hacky way of splitting, because <br> is eaten by .text()
            val infoLines = cols["Rozsah, examinace"]!!.split(" [HT] ")

            SubjectSimple(
                id = cols["Kód"]!!,
                name = cols["Název"]!!,
                semesters = parseSemesters(cols["Semestr"]!!),
                semesterSpecific = parseSemesterSpecificSubjectInfo(infoLines),
                department = cols["Katedra"]!!,
                faculty = cols["Fakulta"]!!,
            )
        }
        return Pair(hasNextPage, data)
    }

    val data = ArrayList<SubjectSimple>()
    var page = 1
    while(true) {
        val (hasNextPage, pageData) = inner(page)
        data.addAll(pageData)
        if(!hasNextPage) {
            break
        }
        page++
    }
    return data
}

@Serializable
sealed class Attendance {
    @Serializable object Unlimited : Attendance()
    @Serializable object Unspecified : Attendance()
    @Serializable data class Limited(val people: Int) : Attendance()
}

fun parseAttendance(str: String): Attendance {
    // TODO: What is the parenthesised part?
    return when(str.split("(")[0].trim()) {
        "neomezen" -> Attendance.Unlimited
        "neurčen" -> Attendance.Unspecified
        else -> Attendance.Limited(str.toInt())
    }
}

@Serializable
data class SubjectAttendance(val lecture: Attendance, val seminar: Attendance)

fun parseSubjectAttendance(str: String) =
    if(str.contains("/")) {
        val split = str.split("/")
        SubjectAttendance(parseAttendance(split[0].trim()), parseAttendance(split[1].trim()))
    } else {
        val attendance = parseAttendance(str)
        SubjectAttendance(attendance, attendance)
    }

@Serializable data class RequisiteSubject(val name: String, val id: String, val points: Int)
@Serializable data class RequisiteGroup(val minPoints: Int, val subjects: List<RequisiteSubject>)

@Serializable
data class SubjectStatistics(
    // TODO: Naming
    val preliminarilyEnrolled: Int,
    val scheduled: Int,
    val enrolledWithoutCompleted: Int,
    val completed: Int,
    val successPercentage: Float,
    val averageMark: Float?,
    val examined: Int?,
    val averageAttempts: Float?,
)

@Serializable data class BilingualText(val czech: String?, val english: String?)

@Serializable
data class SubjectDetail(
    val name: String,
    val id: String,
    val englishName: String,
    val department: NamedWithId,
    val faculty: NamedWithId,
    val validity: String,
    val semesters: Set<Semester>,
    val credits: Int,
    val semesterSpecific: Map<Semester, SemesterSpecificSubjectInfo>,
    val capacity: SubjectAttendance,
    val minStudentNum: SubjectAttendance,
    val isTaught: Boolean, // TODO: Other statuses?
    val language: String,
    val teachingMethod: String,
    val links: List<String>,
    val note: String?,
    val guarantors: List<NamedWithId>,
    val classes: List<NamedWithId>,
    val classificationCategories: List<NamedWithId>,
    val prerequisites: List<RequisiteGroup>,
    val corequisites: List<RequisiteGroup>,
    val incompatibleSubjects: List<NamedWithId>,
    val incompatibleForSubjects: List<NamedWithId>,
    val replacementSubjects: List<NamedWithId>,
    val replacementForSubjects: List<NamedWithId>,
    val annotation: BilingualText,
    val subjectGoals: BilingualText,
    val completionRequirements: BilingualText,
    val literature: BilingualText,
    val teachingMethods: BilingualText,
    val examRequirements: BilingualText,
    val syllabus: BilingualText,
    val entryRequirements: BilingualText,
    val statistics: Map<Pair<AcademicYear, Semester>, SubjectStatistics>,
)

fun getSubject(client: SisClient, id: String): SubjectDetail {
    val doc = client.getPage("predmety/index.php", mapOf("do" to "predmet", "kod" to id))
    val title = doc.selectFirst("#content .form_div_title").text()
    val subjName = title.split(" - ")[0]
    val subjId = title.split(" - ")[1]

    val statisticsTable = doc.select(".tab1").last()
    val years = statisticsTable.select("td[rowspan=2]").map {
        AcademicYear(it.text().split("/")[0].toInt())
    }
    val headers =
        statisticsTable.select(".head2 td")
            .map { it.text().trim() }
            .filter { it != "Akademický rok" }
    val statistics = statisticsTable.select(".row1,.row2").mapIndexedNotNull { j, row ->
        val cols = headers.mapIndexed { i, h ->
            val i2 = if(row.children().size == 10) i + 1 else i
            val v = row.children()[i2].text().trim()
            Pair(h, when(v) {
                "" -> null; "-" -> null; else -> v
            })
        }.toMap()

        if(cols["# předběžně zapsaných"] == null) return@mapIndexedNotNull null

        val year = years[j / 2]
        val semester = when(cols["semestr"]) {
            "zimní" -> Semester.Winter
            "letní" -> Semester.Summer
            else -> throw UnhandledInputException("Semester", cols["semestr"]!!)
        }

        if(cols["# předběžně zapsaných"] == "-") return@mapIndexedNotNull null

        Pair(Pair(year, semester), SubjectStatistics(
            cols["# předběžně zapsaných"]!!.toInt(),
            cols["# v rozvrhu"]!!.toInt(),
            cols["# závazně zapsaných bez uznaných"]!!.toInt(),
            cols["# splněných (více)"]!!.toInt(), // TODO: Also get the extended data
            cols["% úspěšnost"]!!.replace(",", ".").toFloat(),
            cols["průměrná známka"]?.replace(",", ".")?.toFloat(),
            cols["# zkoušení"]?.toInt(),
            cols["průměrný úspěšný termín"]?.replace(",", ".")?.toFloat()))
    }.toMap()

    val kvtables = doc.select("#content .tab2")
    val kv1 = parseKeyValueTableDom(kvtables[0])
    val kv2 = parseKeyValueTableDom(kvtables[1])

    fun getSimplifiedHtml(el: Element): String {
        // The first child is a div that logically belongs to the header
        el.child(0).remove()
        el.allElements.filter { it.text().isBlank() }.forEach { it.remove() }
        el.allElements.forEach { it.removeAttr("style") }
        return el.html()
    }

    fun getText(id: String): BilingualText {
        return BilingualText(
            doc.selectFirst("#" + id + "_CZE")?.let { getSimplifiedHtml(it) },
            doc.selectFirst("#" + id + "_ENG")?.let { getSimplifiedHtml(it) },
        )
    }

    fun parseSubjectLink(link: Element): NamedWithId {
        val js = link.attr("onmousemove")
        val m = Regex("ShowHint\\('hint',this.id,0,'(.*)'\\);").matchEntire(js)
        return NamedWithId(m!!.groupValues[1], getKodFromLink(link))
    }

    fun parseSubjectList(el: Element?): List<NamedWithId> {
        return el?.select("a")?.map { parseSubjectLink(it) } ?: listOf()
    }

    fun parseRequisites(el: Element?): List<RequisiteGroup> {
        return el?.children()?.map { requisiteElem ->
            if(requisiteElem.tagName() == "a") {
                val (name, linkedId) = parseSubjectLink(requisiteElem)
                RequisiteGroup(0, listOf(RequisiteSubject(name, linkedId, 0)))
            } else {
                val span = requisiteElem.selectFirst("span")
                val js = span.attr("onmouseover")
                val m = Regex("ShowHint\\('hint', this.id, 0, '(.*)'\\);").matchEntire(js)
                val innerHtml =
                    m!!.groupValues[1]
                        .replace(Regex("\\\\x([0-9A-Za-z]{2})")) {
                            it.groupValues[1].toInt(16).toChar().toString()
                        }
                val minPts =
                    Regex("Limit pro plnění skupiny: (\\d+)").find(innerHtml)!!.groupValues[1].toInt()

                val innerDoc = Jsoup.parse(innerHtml)
                val subjects = innerDoc.select("tr").map {
                    val idAndName = it.child(0).text().split(" - ")
                    val pts = Regex("(\\d+) body").matchEntire(it.child(1)
                                                                   .text())!!.groupValues[1].toInt()
                    RequisiteSubject(idAndName[1], idAndName[0], pts)
                }

                RequisiteGroup(minPts, subjects)
            }
        } ?: listOf()
    }

    return SubjectDetail(
        name = subjName,
        id = subjId,
        englishName = kv1["Anglický název:"]!!.text(),
        department = NamedWithId(
            kv1["Zajišťuje:"]!!.text(),
            getKodFromLink(kv1["Zajišťuje:"]!!.selectFirst("a"))),
        faculty = NamedWithId(
            kv1["Fakulta:"]!!.text(),
            getKodFromLink(kv1["Fakulta:"]!!.selectFirst("a"))),
        validity = kv1["Platnost:"]!!.text(),
        semesters = parseSemesters(kv1["Semestr:"]!!.text()),
        credits = kv1["E-Kredity:"]!!.text().toInt(),
        semesterSpecific = parseSemesterSpecificSubjectInfo(
            kv1.filterKeys { it.startsWith("Rozsah, examinace:") }
                .values
                .map { it!!.text() }),
        capacity = parseSubjectAttendance(kv1["Počet míst:"]!!.text()),
        minStudentNum = parseSubjectAttendance(kv1["Počet míst:"]!!.text()),
        isTaught = when(val state = kv1["Stav předmětu:"]!!.text()) {
            "vyučován" -> true
            "nevyučován" -> false
			"zrušen" -> false
            else -> throw UnhandledInputException("Stav předmětu", state)
        },
        language = kv1["Jazyk výuky:"]!!.text(),
        teachingMethod = kv1["Způsob výuky:"]!!.text(),
        links = kv1["Další informace:"]
            ?.select("a")
            ?.map { it.attr("href") } ?: listOf(),
        note = kv1["Poznámka:"]?.text(),
        guarantors = kv2["Garant:"]?.select("a")?.map {
            NamedWithId(it.text(), getKodFromLink(it))
        } ?: listOf(),
        classes = kv2["Třída:"]?.select("a")?.map {
            NamedWithId(it.text(), getKodFromLink(it))
        } ?: listOf(),
        classificationCategories = kv2["Klasifikace:"]?.select("a")?.map {
            NamedWithId(it.text(), getKodFromLink(it))
        } ?: listOf(),
        prerequisites = parseRequisites(kv2["Prerekvizity :"]),
        corequisites = parseRequisites(kv2["Korekvizity :"]),
        incompatibleSubjects = parseSubjectList(kv2["Neslučitelnost :"]),
        incompatibleForSubjects = parseSubjectList(kv2["Je neslučitelnost pro:"]),
        replacementSubjects = parseSubjectList(kv2["Záměnnost :"]),
        replacementForSubjects = parseSubjectList(kv2["Je záměnnost pro:"]),
        annotation = getText("pamela_A"),
        subjectGoals = getText("pamela_C"),
        completionRequirements = getText("pamela_E"),
        literature = getText("pamela_L"),
        teachingMethods = getText("pamela_M"),
        examRequirements = getText("pamela_P"),
        syllabus = getText("pamela_S"),
        entryRequirements = getText("pamela_V"),
        statistics = statistics,
    )
}
