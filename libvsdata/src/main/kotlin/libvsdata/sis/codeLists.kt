// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
package libvsdata.sis

import kotlinx.serialization.Serializable

fun getCodeListsKey(client: SisClient): String {
    val doc = client.getPage("index.php")
    val url = doc.select("#hint_ciselniky").attr("href")
    return splitUrlGetParams(url)["KEY"]!!
}

fun getCodeList(client: SisClient, listId: Int): List<Map<String, String?>> {
    val doc = client.getPage("ciselniky/index.php", mapOf(
        "KEY" to getCodeListsKey(client),
        "SERIAL" to "",
        "count" to "-1",
        "id_ciselnik" to listId.toString(),
        "akce" to "cis_v_data_kf",
    ))

    val table = doc.selectFirst(".hp_table_ordered")
    val headers = table.select(".row_head > th").map { it.text() }
    return table.select(".row1,.row2").map { row ->
        headers.mapIndexed { i, h ->
            Pair(h, row.children()[i].text().ifBlank { null })
        }.toMap()
    }
}

// XXX: This will only work on Charles University SIS
// Other instances don't seem to distinguish faculties anyway
@Serializable
data class LocalFaculty(
    val id: String,
    val name: String,
    val shortName: String?,
)

// This is available from the code lists module, but it no longer works anonymously and might be
// disabled soon.
fun getLocalFaculties(client: SisClient): List<LocalFaculty> {
    var doc = client.getPage("rozvrhng/roz_nastaveni.php")
    var table = parseDataTable(doc.selectFirst("#setFakForm table"))
    return table
        .map {
            LocalFaculty(it["Kód"]!!, it["Název"]!!, it["Fakulta"])
        }.filter { it.id != "11000" } // Not actaully a faculty
}
