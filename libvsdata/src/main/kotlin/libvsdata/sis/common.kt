// This file is part of VŠhled and is licenced under the MPL v2.0
// (c) 2021 David Koňařík
package libvsdata.sis

import kotlinx.serialization.Serializable
import libvsdata.AuthenticatedClient
import libvsdata.AuthenticationFailureException
import libvsdata.NotAuthenticatedException
import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.helper.HttpConnection
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.net.URI
import java.util.*

data class UnhandledInputException(val fieldName: String, val value: String) : Exception()

@Serializable
enum class Semester { Winter, Summer }

@Serializable data class AcademicYear(val startYear: Int) {
    override fun toString(): String = "$startYear/${startYear + 1}"
}
@Serializable data class NamedWithId(val name: String, val id: String)

fun splitGetParams(params: String) =
    params.split("&")
        .map { val s = it.split("="); Pair(s[0], s[1]) }
        .toMap()

fun splitUrlGetParams(url: String) = splitGetParams(URI(url).query)

class SisClient(val auth : Authentication) : AuthenticatedClient {
    val baseUrl = "https://is.cuni.cz/studium/"
    var cookies: Map<String, String> = HashMap()
    var idParam: String? = null
    var tidParam: String? = null

    sealed class Authentication {
        object Anonymous : Authentication()
        data class Cas(val username: String, val password: String) : Authentication()
    }

    override fun login() {
        when(this.auth) {
            is Authentication.Anonymous -> {
                val indexResp = Jsoup.connect(this.baseUrl + "index.php").execute()
                this.cookies = indexResp.cookies()
            }
            is Authentication.Cas -> {
                if(this.auth.username.isBlank() || this.auth.password.isBlank()) {
                    throw AuthenticationFailureException()
                }

                val loginResp = Jsoup.connect(this.baseUrl + "login.php").execute()
                this.cookies = loginResp.cookies()
                val loginDoc = loginResp.parse()

                val verifResp =
                    Jsoup.connect(this.baseUrl + "verif.php")
                        .cookies(this.cookies)
                        .data("login", this.auth.username)
                        .data("heslo", this.auth.password)
                        .data("id", loginDoc.selectFirst("input[name=id]").`val`())
                        .data("tstmp", loginDoc.selectFirst("input[name=tstmp]").`val`())
                        .data("accode", loginDoc.selectFirst("input[name=accode]").`val`())
                        .data("sso_autologin", "1")
                        .data("all", "Přihlásit se")
                        .method(Connection.Method.POST)
                        .execute()
                var ssoResp =
                    if(verifResp.url().host == URI(this.baseUrl).host) {
                        // We're directly in SIS
                        verifResp
                    } else {
                        val verifDoc = verifResp.parse()
                        val ssoUrl = verifDoc.selectFirst("#fm1").attr("action")
                        val ssoData = verifDoc.select("#fm1 input").map {
                            HttpConnection.KeyVal.create(it.attr("name"), it.`val`())
                        }

                        Jsoup.connect(ssoUrl)
                            .data(ssoData)
                            .method(Connection.Method.POST)
                            .execute()
                    }

                var endUrl = ssoResp.url()
                if(endUrl.path.matches(Regex(".*/v4/(cs|en)/.*"))) {
                    // User is set up to log in directly to SIS4. This client currently
                    // only deals with SIS3, so we have to switch versions

                    val sis4Doc = ssoResp.parse()
                    val verSwitchUrl = sis4Doc.selectFirst(".-sis3 a").attr("href")
                    val verSwitchResp =
                        // The URL begins with "//", a supported browser feature letting the
                        // browser choose either http or https based on the current protocol
                        // We just hard-code prepend https, but we should really handle this more
                        // generally
                        Jsoup.connect("https:$verSwitchUrl")
                            .cookies(ssoResp.cookies())
                            .execute()
                    endUrl = verSwitchResp.url()
                }

                val endUrlParams = splitGetParams(endUrl.query)
                if(!endUrlParams.containsKey("id")) {
                    throw AuthenticationFailureException()
                }
                this.cookies = ssoResp.cookies()
                this.idParam = endUrlParams["id"]
                this.tidParam = endUrlParams["tid"]
            }
        }
    }

    fun getPageRaw(path: String, params: Map<String, String> = mapOf(),
                   nonHtml: Boolean = false): Connection.Response {
        val newParams = HashMap(params)
        if(this.idParam != null) {
            newParams["id"] = this.idParam
        }
        if(this.tidParam != null) {
            newParams["tid"] = this.tidParam
        }

        val resp =
            Jsoup.connect(this.baseUrl + path)
                // Some pages (long lists) take a long time to load
                // Timeouts should be handled on the UI side
                .timeout(60000000)
                .data(newParams)
                .cookies(this.cookies)
                .ignoreContentType(nonHtml)
                .execute()
        if(resp.url().path.endsWith("/login.php")) {
            throw NotAuthenticatedException()
        }
        return resp
    }

    fun getPage(path: String, params: Map<String, String> = mapOf()): Document {
        val doc = getPageRaw(path, params).parse()
        if(this.auth is Authentication.Cas && !isAuthenticated(doc)) {
            throw NotAuthenticatedException()
        }
        return doc
    }
}

fun isAuthenticated(doc: Document) = doc.select("#flogin").size == 0

fun parseDataTable(table: Element): List<Map<String, String?>> {
    val headers = table
        .selectFirst(".head2,.head1")
        .select("td").map { it.text() }
    return table.select(".row1,.row2").map { row ->
        headers.mapIndexed { i, h ->
            Pair(h, row.children()[i].text().ifBlank { null })
        }.toMap()
    }
}

fun parseKeyValueTableDom(table: Element): Map<String, Element?> {
    var lastKey = ""
    var suffix = 1
    return table.select("tr").map {
        var key = it.child(0).text()
        if(key.isBlank()) {
            suffix++
            key = lastKey + "__" + suffix
        } else {
            lastKey = key
        }
        Pair(key, it.children().getOrNull(1))
    }.toMap()
}

fun parseKeyValueTable(table: Element): Map<String, String?> {
    return parseKeyValueTableDom(table).mapValues { it.value?.text()?.ifBlank { null } }
}

fun getKodFromLink(link: Element): String {
    return splitUrlGetParams(link.attr("href"))["kod"]!!
}

